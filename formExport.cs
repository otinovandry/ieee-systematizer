﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace LibStruct
{
    public partial class formExport : Form
    {
        public enum DocType
        {
            Word, Excel
        }
        private string docFilter = "";
        private List<bool> checkList = new List<bool>();
        public formExport(DocType type)
        {
            InitializeComponent();
            if(type == DocType.Word)
                docFilter = "Документ MS Word (*.docx)|*.docx";
            else
                docFilter = "Документ MS Excel (*.xlsx)|*.xlsx";
        }
        public List<bool> getChecked()
        {
            return checkList;
        }

        public string getPath()
        {
            return tbPath.Text;
        }
        private void btOK_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbPath.Text.Trim()))
            {
                MessageBox.Show("Выберете путь для сохранения фоайла");
                return;
            }
            if (checkedLB.CheckedItems.Count == 0)
            {
                MessageBox.Show("Выберете нужные распределения");
                return;
            }
            foreach (var item in checkedLB.Items)
                checkList.Add(checkedLB.CheckedItems.Contains(item));
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cbCheckAll_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < checkedLB.Items.Count; i++)
                checkedLB.SetItemChecked(i, cbCheckAll.Checked);
        }
        private void btBrowse_Click(object sender, EventArgs e)
        {
            var dlg = new SaveFileDialog();
            dlg.Filter = docFilter;
            if (dlg.ShowDialog() == DialogResult.OK)
                tbPath.Text = dlg.FileName;
        }
    }
}
