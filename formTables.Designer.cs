﻿namespace LibStruct
{
    partial class formTables
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabKeywords = new System.Windows.Forms.TabPage();
            this.keywordsDGV = new System.Windows.Forms.DataGridView();
            this.tabUnivers = new System.Windows.Forms.TabPage();
            this.universDGV = new System.Windows.Forms.DataGridView();
            this.tabCountries = new System.Windows.Forms.TabPage();
            this.countriesDGV = new System.Windows.Forms.DataGridView();
            this.tabTypes = new System.Windows.Forms.TabPage();
            this.rbNA = new System.Windows.Forms.RadioButton();
            this.rbJournals = new System.Windows.Forms.RadioButton();
            this.rbConferences = new System.Windows.Forms.RadioButton();
            this.typesDGV = new System.Windows.Forms.DataGridView();
            this.tabYears = new System.Windows.Forms.TabPage();
            this.yearsDGV = new System.Windows.Forms.DataGridView();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabCount = new System.Windows.Forms.TabPage();
            this.countDGV = new System.Windows.Forms.DataGridView();
            this.btImport = new System.Windows.Forms.Button();
            this.btImportExcel = new System.Windows.Forms.Button();
            this.tabKeywords.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.keywordsDGV)).BeginInit();
            this.tabUnivers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.universDGV)).BeginInit();
            this.tabCountries.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.countriesDGV)).BeginInit();
            this.tabTypes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.typesDGV)).BeginInit();
            this.tabYears.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.yearsDGV)).BeginInit();
            this.tabControl.SuspendLayout();
            this.tabCount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.countDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // tabKeywords
            // 
            this.tabKeywords.BackColor = System.Drawing.Color.Azure;
            this.tabKeywords.Controls.Add(this.keywordsDGV);
            this.tabKeywords.Location = new System.Drawing.Point(4, 25);
            this.tabKeywords.Name = "tabKeywords";
            this.tabKeywords.Size = new System.Drawing.Size(1132, 606);
            this.tabKeywords.TabIndex = 5;
            this.tabKeywords.Text = "По годам и клюевым словам";
            // 
            // keywordsDGV
            // 
            this.keywordsDGV.AllowUserToAddRows = false;
            this.keywordsDGV.AllowUserToDeleteRows = false;
            this.keywordsDGV.AllowUserToResizeColumns = false;
            this.keywordsDGV.AllowUserToResizeRows = false;
            this.keywordsDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.keywordsDGV.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.keywordsDGV.BackgroundColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.keywordsDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.keywordsDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle20.BackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.keywordsDGV.DefaultCellStyle = dataGridViewCellStyle20;
            this.keywordsDGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.keywordsDGV.EnableHeadersVisualStyles = false;
            this.keywordsDGV.Location = new System.Drawing.Point(0, 0);
            this.keywordsDGV.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.keywordsDGV.Name = "keywordsDGV";
            this.keywordsDGV.ReadOnly = true;
            this.keywordsDGV.RowHeadersVisible = false;
            this.keywordsDGV.RowHeadersWidth = 51;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.keywordsDGV.RowsDefaultCellStyle = dataGridViewCellStyle21;
            this.keywordsDGV.RowTemplate.Height = 24;
            this.keywordsDGV.Size = new System.Drawing.Size(1132, 606);
            this.keywordsDGV.TabIndex = 14;
            // 
            // tabUnivers
            // 
            this.tabUnivers.BackColor = System.Drawing.Color.Azure;
            this.tabUnivers.Controls.Add(this.universDGV);
            this.tabUnivers.Location = new System.Drawing.Point(4, 25);
            this.tabUnivers.Name = "tabUnivers";
            this.tabUnivers.Size = new System.Drawing.Size(1132, 606);
            this.tabUnivers.TabIndex = 4;
            this.tabUnivers.Text = "По годам и уч. заведениям";
            // 
            // universDGV
            // 
            this.universDGV.AllowUserToAddRows = false;
            this.universDGV.AllowUserToDeleteRows = false;
            this.universDGV.AllowUserToResizeColumns = false;
            this.universDGV.AllowUserToResizeRows = false;
            this.universDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.universDGV.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.universDGV.BackgroundColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.universDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this.universDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.universDGV.DefaultCellStyle = dataGridViewCellStyle23;
            this.universDGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.universDGV.EnableHeadersVisualStyles = false;
            this.universDGV.Location = new System.Drawing.Point(0, 0);
            this.universDGV.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.universDGV.Name = "universDGV";
            this.universDGV.ReadOnly = true;
            this.universDGV.RowHeadersVisible = false;
            this.universDGV.RowHeadersWidth = 51;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.universDGV.RowsDefaultCellStyle = dataGridViewCellStyle24;
            this.universDGV.RowTemplate.Height = 24;
            this.universDGV.Size = new System.Drawing.Size(1132, 606);
            this.universDGV.TabIndex = 13;
            // 
            // tabCountries
            // 
            this.tabCountries.BackColor = System.Drawing.Color.Azure;
            this.tabCountries.Controls.Add(this.countriesDGV);
            this.tabCountries.Location = new System.Drawing.Point(4, 25);
            this.tabCountries.Name = "tabCountries";
            this.tabCountries.Padding = new System.Windows.Forms.Padding(3);
            this.tabCountries.Size = new System.Drawing.Size(1132, 606);
            this.tabCountries.TabIndex = 3;
            this.tabCountries.Text = "По странам и годам";
            // 
            // countriesDGV
            // 
            this.countriesDGV.AllowUserToAddRows = false;
            this.countriesDGV.AllowUserToDeleteRows = false;
            this.countriesDGV.AllowUserToResizeColumns = false;
            this.countriesDGV.AllowUserToResizeRows = false;
            this.countriesDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.countriesDGV.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.countriesDGV.BackgroundColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle25.BackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.countriesDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this.countriesDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle26.BackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.countriesDGV.DefaultCellStyle = dataGridViewCellStyle26;
            this.countriesDGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.countriesDGV.EnableHeadersVisualStyles = false;
            this.countriesDGV.Location = new System.Drawing.Point(3, 3);
            this.countriesDGV.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.countriesDGV.Name = "countriesDGV";
            this.countriesDGV.ReadOnly = true;
            this.countriesDGV.RowHeadersVisible = false;
            this.countriesDGV.RowHeadersWidth = 51;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.countriesDGV.RowsDefaultCellStyle = dataGridViewCellStyle27;
            this.countriesDGV.RowTemplate.Height = 24;
            this.countriesDGV.Size = new System.Drawing.Size(1126, 600);
            this.countriesDGV.TabIndex = 12;
            // 
            // tabTypes
            // 
            this.tabTypes.BackColor = System.Drawing.Color.Azure;
            this.tabTypes.Controls.Add(this.rbNA);
            this.tabTypes.Controls.Add(this.rbJournals);
            this.tabTypes.Controls.Add(this.rbConferences);
            this.tabTypes.Controls.Add(this.typesDGV);
            this.tabTypes.Location = new System.Drawing.Point(4, 25);
            this.tabTypes.Name = "tabTypes";
            this.tabTypes.Padding = new System.Windows.Forms.Padding(3);
            this.tabTypes.Size = new System.Drawing.Size(1132, 606);
            this.tabTypes.TabIndex = 2;
            this.tabTypes.Text = "По годам и типам";
            // 
            // rbNA
            // 
            this.rbNA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.rbNA.AutoSize = true;
            this.rbNA.Location = new System.Drawing.Point(1003, 568);
            this.rbNA.Name = "rbNA";
            this.rbNA.Size = new System.Drawing.Size(123, 21);
            this.rbNA.TabIndex = 14;
            this.rbNA.Text = "Тип не указан";
            this.rbNA.UseVisualStyleBackColor = true;
            this.rbNA.UseWaitCursor = true;
            this.rbNA.CheckedChanged += new System.EventHandler(this.rbNA_CheckedChanged);
            // 
            // rbJournals
            // 
            this.rbJournals.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.rbJournals.AutoSize = true;
            this.rbJournals.Location = new System.Drawing.Point(1005, 514);
            this.rbJournals.Name = "rbJournals";
            this.rbJournals.Size = new System.Drawing.Size(91, 21);
            this.rbJournals.TabIndex = 13;
            this.rbJournals.Text = "Журналы";
            this.rbJournals.UseVisualStyleBackColor = true;
            this.rbJournals.CheckedChanged += new System.EventHandler(this.rbJournals_CheckedChanged);
            // 
            // rbConferences
            // 
            this.rbConferences.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.rbConferences.AutoSize = true;
            this.rbConferences.Checked = true;
            this.rbConferences.Location = new System.Drawing.Point(1005, 541);
            this.rbConferences.Name = "rbConferences";
            this.rbConferences.Size = new System.Drawing.Size(121, 21);
            this.rbConferences.TabIndex = 12;
            this.rbConferences.TabStop = true;
            this.rbConferences.Text = "Конференции";
            this.rbConferences.UseVisualStyleBackColor = true;
            this.rbConferences.CheckedChanged += new System.EventHandler(this.rbConferences_CheckedChanged);
            // 
            // typesDGV
            // 
            this.typesDGV.AllowUserToAddRows = false;
            this.typesDGV.AllowUserToDeleteRows = false;
            this.typesDGV.AllowUserToResizeColumns = false;
            this.typesDGV.AllowUserToResizeRows = false;
            this.typesDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.typesDGV.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.typesDGV.BackgroundColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle28.BackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle28.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle28.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.typesDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle28;
            this.typesDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle29.BackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle29.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle29.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.typesDGV.DefaultCellStyle = dataGridViewCellStyle29;
            this.typesDGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.typesDGV.EnableHeadersVisualStyles = false;
            this.typesDGV.Location = new System.Drawing.Point(3, 3);
            this.typesDGV.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.typesDGV.Name = "typesDGV";
            this.typesDGV.ReadOnly = true;
            this.typesDGV.RowHeadersVisible = false;
            this.typesDGV.RowHeadersWidth = 51;
            dataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.typesDGV.RowsDefaultCellStyle = dataGridViewCellStyle30;
            this.typesDGV.RowTemplate.Height = 24;
            this.typesDGV.Size = new System.Drawing.Size(1126, 600);
            this.typesDGV.TabIndex = 11;
            // 
            // tabYears
            // 
            this.tabYears.BackColor = System.Drawing.Color.Azure;
            this.tabYears.Controls.Add(this.yearsDGV);
            this.tabYears.Location = new System.Drawing.Point(4, 25);
            this.tabYears.Name = "tabYears";
            this.tabYears.Padding = new System.Windows.Forms.Padding(3);
            this.tabYears.Size = new System.Drawing.Size(1132, 606);
            this.tabYears.TabIndex = 1;
            this.tabYears.Text = "По годам(все)";
            // 
            // yearsDGV
            // 
            this.yearsDGV.AllowUserToAddRows = false;
            this.yearsDGV.AllowUserToDeleteRows = false;
            this.yearsDGV.AllowUserToResizeColumns = false;
            this.yearsDGV.AllowUserToResizeRows = false;
            this.yearsDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.yearsDGV.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.yearsDGV.BackgroundColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle31.BackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle31.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.yearsDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle31;
            this.yearsDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle32.BackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle32.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle32.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle32.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.yearsDGV.DefaultCellStyle = dataGridViewCellStyle32;
            this.yearsDGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.yearsDGV.EnableHeadersVisualStyles = false;
            this.yearsDGV.Location = new System.Drawing.Point(3, 3);
            this.yearsDGV.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.yearsDGV.Name = "yearsDGV";
            this.yearsDGV.ReadOnly = true;
            this.yearsDGV.RowHeadersVisible = false;
            this.yearsDGV.RowHeadersWidth = 51;
            dataGridViewCellStyle33.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.yearsDGV.RowsDefaultCellStyle = dataGridViewCellStyle33;
            this.yearsDGV.RowTemplate.Height = 24;
            this.yearsDGV.Size = new System.Drawing.Size(1126, 600);
            this.yearsDGV.TabIndex = 2;
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.tabYears);
            this.tabControl.Controls.Add(this.tabTypes);
            this.tabControl.Controls.Add(this.tabCountries);
            this.tabControl.Controls.Add(this.tabUnivers);
            this.tabControl.Controls.Add(this.tabKeywords);
            this.tabControl.Controls.Add(this.tabCount);
            this.tabControl.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tabControl.Location = new System.Drawing.Point(-2, 1);
            this.tabControl.MinimumSize = new System.Drawing.Size(1140, 635);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1140, 635);
            this.tabControl.TabIndex = 2;
            // 
            // tabCount
            // 
            this.tabCount.Controls.Add(this.countDGV);
            this.tabCount.Location = new System.Drawing.Point(4, 25);
            this.tabCount.Name = "tabCount";
            this.tabCount.Size = new System.Drawing.Size(1132, 606);
            this.tabCount.TabIndex = 6;
            this.tabCount.Text = "По годам и кол-ву авторов";
            this.tabCount.UseVisualStyleBackColor = true;
            // 
            // countDGV
            // 
            this.countDGV.AllowUserToAddRows = false;
            this.countDGV.AllowUserToDeleteRows = false;
            this.countDGV.AllowUserToResizeColumns = false;
            this.countDGV.AllowUserToResizeRows = false;
            this.countDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.countDGV.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.countDGV.BackgroundColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle34.BackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle34.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle34.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle34.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle34.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle34.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.countDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle34;
            this.countDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle35.BackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle35.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle35.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle35.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle35.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.countDGV.DefaultCellStyle = dataGridViewCellStyle35;
            this.countDGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.countDGV.EnableHeadersVisualStyles = false;
            this.countDGV.Location = new System.Drawing.Point(0, 0);
            this.countDGV.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.countDGV.Name = "countDGV";
            this.countDGV.ReadOnly = true;
            this.countDGV.RowHeadersVisible = false;
            this.countDGV.RowHeadersWidth = 51;
            dataGridViewCellStyle36.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.countDGV.RowsDefaultCellStyle = dataGridViewCellStyle36;
            this.countDGV.RowTemplate.Height = 24;
            this.countDGV.Size = new System.Drawing.Size(1132, 606);
            this.countDGV.TabIndex = 15;
            // 
            // btImport
            // 
            this.btImport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btImport.Location = new System.Drawing.Point(966, 0);
            this.btImport.Name = "btImport";
            this.btImport.Size = new System.Drawing.Size(81, 28);
            this.btImport.TabIndex = 3;
            this.btImport.Text = "в Word";
            this.btImport.UseVisualStyleBackColor = true;
            this.btImport.Click += new System.EventHandler(this.btImportWord_Click);
            // 
            // btImportExcel
            // 
            this.btImportExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btImportExcel.Location = new System.Drawing.Point(1050, 0);
            this.btImportExcel.Name = "btImportExcel";
            this.btImportExcel.Size = new System.Drawing.Size(81, 28);
            this.btImportExcel.TabIndex = 4;
            this.btImportExcel.Text = "в Excel";
            this.btImportExcel.UseVisualStyleBackColor = true;
            this.btImportExcel.Click += new System.EventHandler(this.btImportExcel_Click);
            // 
            // formTables
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1142, 633);
            this.Controls.Add(this.btImportExcel);
            this.Controls.Add(this.btImport);
            this.Controls.Add(this.tabControl);
            this.MinimumSize = new System.Drawing.Size(1160, 680);
            this.Name = "formTables";
            this.Text = "Таблицы";
            this.tabKeywords.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.keywordsDGV)).EndInit();
            this.tabUnivers.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.universDGV)).EndInit();
            this.tabCountries.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.countriesDGV)).EndInit();
            this.tabTypes.ResumeLayout(false);
            this.tabTypes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.typesDGV)).EndInit();
            this.tabYears.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.yearsDGV)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.tabCount.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.countDGV)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabKeywords;
        private System.Windows.Forms.TabPage tabUnivers;
        private System.Windows.Forms.TabPage tabCountries;
        private System.Windows.Forms.TabPage tabTypes;
        private System.Windows.Forms.DataGridView typesDGV;
        private System.Windows.Forms.TabPage tabYears;
        private System.Windows.Forms.DataGridView yearsDGV;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.RadioButton rbJournals;
        private System.Windows.Forms.RadioButton rbConferences;
        private System.Windows.Forms.DataGridView countriesDGV;
        private System.Windows.Forms.DataGridView universDGV;
        private System.Windows.Forms.DataGridView keywordsDGV;
        private System.Windows.Forms.TabPage tabCount;
        private System.Windows.Forms.DataGridView countDGV;
        private System.Windows.Forms.Button btImport;
        private System.Windows.Forms.Button btImportExcel;
        private System.Windows.Forms.RadioButton rbNA;
    }
}