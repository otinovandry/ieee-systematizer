﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Xceed.Document.NET;
using Xceed.Words.NET;
using Chart = System.Windows.Forms.DataVisualization.Charting.Chart;
namespace LibStruct
{
    public partial class formCharts : Form
    {
        SortedDictionary<string, int> years = new SortedDictionary<string, int>();
        List<Record> allowedList;
        formFilter search;
        public formCharts()
        {
            allowedList = new List<Record>(formMain.allowedList);
            search = formMain.currentSearh;
            if (search != null && search.YearFor != "" && search.YearTo != "")
                for (int i = int.Parse(search.YearFor); i <= int.Parse(search.YearTo); i++)
                    years.Add(i.ToString(), 0);
            InitializeComponent();
            InitializeYearsDiagramPie();
            InitializeTypesDiagram("Conference");
            InitializeCountriesDiagram();
            InitializeAffilationsDiagram();
            InitializeKeyWordsDiagram();
            InitializeCountDiagram();
        }

        public formCharts(string path, bool useYears, bool useConferences, bool useJournals, bool useNA,
            bool useCountries, bool useAffilations, bool useKeywords, bool useCount, bool isWord)
        {
            var progress = new formProgress(8);
            progress.Show();
            allowedList = new List<Record>(formMain.allowedList);
            search = formMain.currentSearh;
            if (search != null && search.YearFor != "" && search.YearTo != "")
                for (int i = int.Parse(search.YearFor); i <= int.Parse(search.YearTo); i++)
                    years.Add(i.ToString(), 0);
            else
                foreach (var rec in allowedList)
                    if (!years.Keys.Contains(rec.PublicationYear))
                        years.Add(rec.PublicationYear, 0);

            // создаём документ
            DocX document = DocX.Create(path);
            document.SetDefaultFont(new Font("Times New Roman"), 14, null);

            // заполнение документа диаграммами
            if (useYears)
                ImportYearsDiagram(document, "По годам");
            progress.nextStep();
            if (useConferences)
                ImportTypesDiagram(document, "По конференциям", "Conference");
            progress.nextStep();
            if (useJournals)
                ImportTypesDiagram(document, "По журналам", "Journal");
            progress.nextStep();
            if (useNA)
                ImportTypesDiagram(document, "По неуказанному типу", "Not defined");
            progress.nextStep();
            if (useCountries)
                ImportCountriesDiagram(document, "По странам");
            progress.nextStep();
            if (useAffilations)
                ImportAffilationsDiagram(document, "По учебным заведениям");
            progress.nextStep();
            if (useKeywords)
                ImportKeywordsDiagram(document, "По ключевым словам");
            progress.nextStep();
            if (useCount)
                ImportCountDiagram(document, "По численному составу авторов");
            progress.nextStep();

            try
            { document.Save(); }
            catch (Exception)
            {
                MessageBox.Show("Проверьте разрешения доступа к указанному пути и антивирус\n" +
                    "Можете попробовать перезапустить программу с правами администратора",
                    "Ошибка сохранения документа",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            progress.Close();
        }

        void ImportYearsDiagram(DocX document, string diagramName)
        {
            years.Keys.ToList().ForEach(year => years[year] = 0);

            foreach (var rec in allowedList)
                if (years.Keys.Contains(rec.PublicationYear))
                    years[rec.PublicationYear]++;
                else years.Add(rec.PublicationYear, 1);

            BarChart chart = new BarChart();
            Series seria = new Series(diagramName);
            List<Tuple<string, int>> data = new List<Tuple<string, int>>();

            foreach (var year in years)
                data.Add(new Tuple<string, int>(year.Key, year.Value));
            seria.Bind(data, "Item1", "Item2");
            chart.AddSeries(seria);

            document.InsertChart(chart);
            document.InsertParagraph().AppendLine();
        }

        void ImportTypesDiagram(DocX document, string diagramName, string type)
        {
            years.Keys.ToList().ForEach(year => years[year] = 0);
            BarChart chart = new BarChart();

            allowedList.Where(rec => rec.DocumentIdentifier == type).ToList()
                    .ForEach(rec =>
                    {
                        if (years.Keys.Contains(rec.PublicationYear))
                            years[rec.PublicationYear]++;
                        else
                            years.Add(rec.PublicationYear, 1);
                    });

            Series seria = new Series(diagramName);
            List<Tuple<string, int>> data = new List<Tuple<string, int>>();

            foreach (var year in years)
                data.Add(new Tuple<string, int>(year.Key, year.Value));
            seria.Bind(data, "Item1", "Item2");
            chart.AddSeries(seria);

            document.InsertChart(chart);
            document.InsertParagraph().AppendLine();
        }

        void ImportCountriesDiagram(DocX document, string diagramName)
        {
            years.Keys.ToList().ForEach(year => years[year] = 0);
            BarChart chart = new BarChart();
            chart.AddLegend(ChartLegendPosition.Top, false);

            List<string> countries = new List<string>();
            if (search != null && search.CheckedCountries.Count != 0)
                countries = search.CheckedCountries;
            else
                foreach (var rec in allowedList)
                    countries = countries.Union(rec.countries).ToList();
            countries = countries.Distinct().ToList();

            foreach (var country in countries)
            {
                Series seria = new Series(country);
                List<Tuple<string, int>> data = new List<Tuple<string, int>>();

                foreach (var year in years)
                {
                    int count = 0;
                    foreach (var rec in allowedList)
                        if (rec.PublicationYear.Equals(year.Key) && rec.countries.Contains(country))
                            count++;
                    data.Add(new Tuple<string, int>(year.Key, count));
                }
                seria.Bind(data, "Item1", "Item2");
                chart.AddSeries(seria);
            }
            document.InsertParagraph().Append(diagramName);
            if (chart.Series.Count > 25)
                document.InsertParagraph().Append("Внимание! В легенде могут быть отображены не все страны - их слишком много");
            document.InsertChart(chart);
            document.InsertParagraph().AppendLine();
        }

        void ImportAffilationsDiagram(DocX document, string diagramName)
        {
            years.Keys.ToList().ForEach(year => years[year] = 0);
            BarChart chart = new BarChart();
            chart.AddLegend(ChartLegendPosition.Top, false);

            List<string> affilations = new List<string>();
            if (search != null && search.CheckedUniversities.Count != 0)
                affilations = search.CheckedUniversities;
            else
                foreach (var rec in allowedList)
                    affilations = affilations.Union(rec.affilations).ToList();
            affilations = affilations.Distinct().ToList();

            if (affilations.Count > 150 &&
                MessageBox.Show("Диаграмма \"" + diagramName + "\" содержит слишком много данных. " +
                "Конечный документ может не открыться на вашем ПК\nОсуществить её импорт?",
                "Требуется ваше решение", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;


            foreach (var affilation in affilations)
            {
                Series seria = new Series(affilation);
                List<Tuple<string, int>> data = new List<Tuple<string, int>>();

                foreach (var year in years)
                {
                    int count = 0;
                    foreach (var rec in allowedList)
                        if (rec.PublicationYear.Equals(year.Key) && rec.affilations.Contains(affilation))
                            count++;
                    data.Add(new Tuple<string, int>(year.Key, count));
                }
                seria.Bind(data, "Item1", "Item2");
                chart.AddSeries(seria);
            }

            document.InsertParagraph().Append(diagramName);
            if (chart.Series.Count > 7)
                document.InsertParagraph().Append("Внимание! В легенде могут быть отображены не все учебные заведения");
            document.InsertChart(chart);
            document.InsertParagraph().AppendLine();
        }

        void ImportKeywordsDiagram(DocX document, string diagramName)
        {
            if (search == null || search.Keywords.Count == 0) return;

            BarChart chart = new BarChart();
            chart.AddLegend(ChartLegendPosition.Top, false);

            foreach (var kw in search.Keywords)
            {
                Series seria = new Series(kw);
                List<Tuple<string, int>> data = new List<Tuple<string, int>>();

                foreach (var year in years)
                {
                    int count = 0;
                    foreach (var rec in allowedList)
                    {
                        if (!rec.PublicationYear.Equals(year.Key)) continue;
                        bool includeIt = false;
                        foreach (var dataKW in rec.keywords)
                            includeIt = includeIt || dataKW.Contains(kw.ToUpper());
                        if (includeIt)
                            count++;
                    }
                    data.Add(new Tuple<string, int>(year.Key, count));
                }
                seria.Bind(data, "Item1", "Item2");
                chart.AddSeries(seria);
            }

            document.InsertParagraph().Append(diagramName);
            if (chart.Series.Count > 28)
                document.InsertParagraph().Append("Внимание! В легенде отображены не все ключевые слова - их слишком много");
            document.InsertChart(chart);
            document.InsertParagraph().AppendLine();
        }
        void ImportCountDiagram(DocX document, string diagramName)
        {
            BarChart chart = new BarChart();
            chart.AddLegend(ChartLegendPosition.Top, false);

            List<int> authCounts = new List<int>();
            foreach (var rec in allowedList)
                if (!authCounts.Contains(rec.authors.Count)) authCounts.Add(rec.authors.Count);

            foreach (var aCount in authCounts)
            {
                Series seria = new Series(aCount + " человек");
                List<Tuple<string, int>> data = new List<Tuple<string, int>>();
                foreach (var year in years)
                {
                    int count = 0;
                    foreach (var rec in allowedList)
                        if (rec.PublicationYear.Equals(year.Key) && rec.authors.Count == aCount)
                            count++;
                    data.Add(new Tuple<string, int>(year.Key, count));
                }
                seria.Bind(data, "Item1", "Item2");
                chart.AddSeries(seria);
            }

            document.InsertParagraph().Append(diagramName);
            if (chart.Series.Count > 28)
                document.InsertParagraph().Append("Внимание! В легенде отображены не все ключевые слова - их слишком много");
            document.InsertChart(chart);
            document.InsertParagraph().AppendLine();
        }
        void InitializeYearsDiagramPie()  // по годам круговая
        {
            try
            {
                chartForYears.Series.Clear();
                chartForYears.Series.Add("");
                chartForYears.Legends.Add("");
                chartForYears.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;

                years.Keys.ToList().ForEach(year => years[year] = 0);

                foreach (var rec in allowedList)
                    if (years.Keys.Contains(rec.PublicationYear))
                        years[rec.PublicationYear]++;
                    else years.Add(rec.PublicationYear, 1);

                int i = 0;
                foreach (var year in years)
                {
                    chartForYears.Series[0].Points.AddY(year.Value);
                    chartForYears.Series[0].Points[i].LegendText = year.Key + " год - " + year.Value;
                    chartForYears.Series[0].Points[i].ToolTip = year.Key + " год - " + year.Value;
                    i++;
                }
            }
            catch (Exception) { tabYears.Parent = null; }
        }

        void InitializeYearsDiagramColumns()  // по годам ступенчатая
        {
            try
            {
                chartForYears.Series.Clear();
                chartForYears.Series.Add("");
                chartForYears.Legends.Clear();
                chartForYears.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
                chartForYears.Legends.Clear();

                years.Keys.ToList().ForEach(year => years[year] = 0);

                foreach (var rec in allowedList)
                    if (years.Keys.Contains(rec.PublicationYear))
                        years[rec.PublicationYear]++;
                    else years.Add(rec.PublicationYear, 1);

                foreach (var year in years)
                {
                    chartForYears.Series[0].Points.AddXY(int.Parse(year.Key), year.Value);
                    chartForYears.Series[0].Points.Last().ToolTip = year.Key + " год - " + year.Value;
                }

                EnableChartZoom(chartForYears);
            }
            catch (Exception) { tabYears.Parent = null; }
        }

        void InitializeTypesDiagram(string type)  //  по конференциям
        {
            try
            {
                chartForTypes.Series.Clear();
                chartForTypes.Series.Add("");
                chartForTypes.ChartAreas.Clear();
                chartForTypes.ChartAreas.Add("");
                chartForTypes.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
                chartForTypes.Legends.Clear();
                years.Keys.ToList().ForEach(year => years[year] = 0);

                allowedList.Where(rec => rec.DocumentIdentifier == type).ToList()
                    .ForEach(rec =>
                    {
                        if (years.Keys.Contains(rec.PublicationYear))
                            years[rec.PublicationYear]++;
                        else
                            years.Add(rec.PublicationYear, 1);
                    });

                foreach (var year in years)
                    chartForTypes.Series[0].Points.AddXY(int.Parse(year.Key), year.Value);

                EnableChartZoom(chartForTypes);
            }
            catch (Exception) { tabTypes.Parent = null; }
        }

        void InitializeCountriesDiagram()  // по странам
        {
            ClearColumnChart(chartForCountries);
            try
            {
                List<string> countries = new List<string>();
                if (search != null && search.CheckedCountries.Count != 0)
                    countries = search.CheckedCountries;
                else
                    foreach (var rec in allowedList)
                        countries = countries.Union(rec.countries).ToList();
                countries = countries.Distinct().ToList();

                int i = 0;
                foreach (var country in countries)
                {
                    chartForCountries.Series.Add(i.ToString());
                    chartForCountries.Series[i].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
                    foreach (var year in years)
                    {
                        int count = 0;
                        foreach (var rec in allowedList)
                            if (rec.PublicationYear.Equals(year.Key) && rec.countries.Contains(country))
                                count++;
                        chartForCountries.Series[i].Points.AddXY(int.Parse(year.Key), count);
                    }
                    chartForCountries.Series[i].ToolTip =
                    chartForCountries.Series[i].LegendText = country;
                    i++;
                }

                EnableChartZoom(chartForCountries);
            }
            catch (Exception) { tabCountries.Parent = null; }
        }

        void InitializeAffilationsDiagram()  // по учебным заведениям
        {
            ClearColumnChart(chartForAffilations);
            try
            {
                List<string> affilations = new List<string>();
                if (search != null && search.CheckedUniversities.Count != 0)
                    affilations = search.CheckedUniversities;
                else
                    foreach (var rec in allowedList)
                        affilations = affilations.Union(rec.affilations).ToList();
                affilations = affilations.Distinct().ToList();

                int i = 0;
                foreach (var affilation in affilations)
                {
                    chartForAffilations.Series.Add(i.ToString());
                    chartForAffilations.Series[i].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
                    foreach (var year in years)
                    {
                        int count = 0;
                        foreach (var rec in allowedList)
                            if (rec.PublicationYear.Equals(year.Key) && rec.affilations.Contains(affilation))
                                count++;
                        chartForAffilations.Series[i].Points.AddXY(int.Parse(year.Key), count);
                        chartForAffilations.Series[i].ToolTip = affilation;
                    }
                    chartForAffilations.Series[i].LegendText = affilation;
                    i++;
                }

                EnableChartZoom(chartForAffilations);
            }
            catch (Exception) { tabUnivers.Parent = null; }
        }


        void InitializeKeyWordsDiagram()  // по ключевым словам
        {
            ClearColumnChart(chartForKeywords);
            try
            {
                if (search == null || search.Keywords.Count == 0) throw new Exception();

                int i = 0;
                foreach (var kw in search.Keywords)
                {
                    chartForKeywords.Series.Add(i.ToString());
                    chartForKeywords.Series[i].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
                    foreach (var year in years)
                    {
                        int count = 0;
                        foreach (var rec in allowedList)
                        {
                            if (!rec.PublicationYear.Equals(year.Key)) continue;
                            bool includeIt = false;
                            foreach (var dataKW in rec.keywords)
                                includeIt = includeIt || dataKW.Contains(kw.ToUpper());
                            if (includeIt)
                                count++;
                        }
                        chartForKeywords.Series[i].Points.AddXY(int.Parse(year.Key), count);
                        chartForKeywords.Series[i].ToolTip = kw;
                    }
                    chartForKeywords.Series[i].LegendText = kw;
                    i++;
                }

                EnableChartZoom(chartForKeywords);
            }
            catch (Exception) { tabKeywords.Parent = null; }
        }

        void InitializeCountDiagram()  // по кол-ву авторов
        {
            ClearColumnChart(chartForCount);
            try
            {
                List<int> authCounts = new List<int>();
                foreach (var rec in allowedList)
                    if (!authCounts.Contains(rec.authors.Count)) authCounts.Add(rec.authors.Count);

                int i = 0;
                foreach (var aCount in authCounts)
                {
                    chartForCount.Series.Add(i.ToString());
                    chartForCount.Series[i].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
                    foreach (var year in years)
                    {
                        int count = 0;
                        foreach (var rec in allowedList)
                            if (rec.PublicationYear.Equals(year.Key) && rec.authors.Count == aCount)
                                count++;
                        chartForCount.Series[i].Points.AddXY(int.Parse(year.Key), count);
                        chartForCount.Series[i].ToolTip = aCount.ToString();
                    }
                    chartForCount.Series[i].LegendText = aCount.ToString();
                    i++;
                }
                EnableChartZoom(chartForCount);
            }
            catch (Exception) { tabCount.Parent = null; }
        }

        private void ClearColumnChart(Chart chart)
        {
            chart.Series.Clear();
            chart.ChartAreas.Clear();
            chart.ChartAreas.Add("");
            chart.Legends.Clear();
            chart.Legends.Add("");
        }
        private void EnableChartZoom(Chart chart)
        {
            chart.ChartAreas[0].CursorX.IsUserEnabled =
            chart.ChartAreas[0].CursorX.IsUserSelectionEnabled =
            chart.ChartAreas[0].AxisX.ScaleView.Zoomable =
            chart.ChartAreas[0].AxisX.ScrollBar.IsPositionedInside =
            chart.ChartAreas[0].CursorY.IsUserEnabled =
            chart.ChartAreas[0].CursorY.IsUserSelectionEnabled =
            chart.ChartAreas[0].AxisY.ScaleView.Zoomable =
            chart.ChartAreas[0].AxisY.ScrollBar.IsPositionedInside = true;
        }

        private void rbColumn_CheckedChanged(object sender, EventArgs e)
        {
            InitializeYearsDiagramColumns();
        }

        private void rbPie_CheckedChanged(object sender, EventArgs e)
        {
            InitializeYearsDiagramPie();
        }
        private void rbJournals_CheckedChanged(object sender, EventArgs e)
        {
            InitializeTypesDiagram("Journal");
        }

        private void rbConferences_CheckedChanged(object sender, EventArgs e)
        {
            InitializeTypesDiagram("Conference");
        }

        private void rbNA_CheckedChanged(object sender, EventArgs e)
        {
            InitializeTypesDiagram("Not defined");
        }
    }
}
