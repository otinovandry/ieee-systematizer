﻿using LibStruct.Properties;
using System;
using System.Reflection;
using System.Windows.Forms;

namespace LibStruct
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            AppDomain.CurrentDomain.AssemblyResolve += AppDomain_AssemblyResolve;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new formMain());
        }
        private static Assembly AppDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            if (args.Name.Contains("Xceed.Words.NET")) //имя dll
                return Assembly.Load(Resources.Xceed_Words_NET); //расположения dll в папке ресурсы
            if (args.Name.Contains("Xceed.Document.NET")) //имя dll
                return Assembly.Load(Resources.Xceed_Document_NET); //расположения dll в папке ресурсы
            if (args.Name.Contains("EPPlus")) //имя dll
                return Assembly.Load(Resources.EPPlus); //расположения dll в папке ресурсы
            return null;
        }
    }
}
