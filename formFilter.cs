﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace LibStruct
{
    public partial class formFilter : Form
    {
        public formFilter(Dictionary<string, Record> clearData)
        {
            List<string> countries = new List<string>();
            List<string> universities = new List<string>();

            foreach (var rec in clearData)
            {
                countries = countries.Union(rec.Value.countries).ToList();
                universities = universities.Union(rec.Value.affilations).ToList();
            }

            countries = countries.Distinct().ToList();
            universities = universities.Distinct().ToList();

            countries.Sort();
            universities.Sort();
            InitializeComponent();
            cbSources.SelectedIndex = 0;
            foreach (var country in countries)
            {
                if (country != "NA" && country != "...")
                    checkListCountries.Items.Add(country);
                //else checkListCountries.Items.Add("Не определена");
            }
            foreach (var university in universities)
            {
                if (university != "NA" && university != "...")
                    checkListUnivers.Items.Add(university);
                //else checkListUnivers.Items.Add("Не определено");
            }
        }
        public string YearFor
        {
            get
            {
                if (checkbYears.Checked)
                    return tbYearFor.Text;
                return "";

            }
        }
        public string YearTo
        {
            get
            {
                if (checkbYears.Checked)
                    return tbYearTo.Text;
                return "";
            }
        }
        public string Sources
        {
            get
            {
                switch (cbSources.SelectedIndex)
                {
                    case 1: return "Journal";
                    case 2: return "Conference";
                    default: return "All";
                }
            }
        }
        public List<string> CheckedCountries
        {
            get
            {
                List<string> check = new List<string>();
                foreach (string s in checkListCountries.CheckedItems.OfType<string>())
                {
                    check.Add(s);
                }
                return check;
            }
        }

        public List<string> CheckedUniversities
        {
            get
            {
                var check = new List<string>();
                foreach (string s in checkListUnivers.CheckedItems.OfType<string>())
                    check.Add(s);
                return check;
            }
        }
        public List<string> Keywords
        {
            get
            {
                var keywords = new List<string>();
                foreach (var keyword in tbKeywords.Text.Split(','))
                    if (keyword.Trim() != "")
                        keywords.Add(keyword.Trim());
                return keywords;
            }
        }
        private void checkListCountries_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkListCountries.ClearSelected();
        }

        private void checkListUnivers_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkListUnivers.ClearSelected();
        }

        private void cbYears_CheckedChanged(object sender, EventArgs e)
        {
            tbYearFor.Visible = tbYearTo.Visible = lbYears.Visible = checkbYears.Checked;
            tbYearFor.Text = tbYearTo.Text = "";
        }

        private void tbKeywords_TextChanged(object sender, EventArgs e)
        {
            tbKeywords.Text = Regex.Replace(tbKeywords.Text, "[^ 0-9a-zA-Zа-яА-Я,-]", "");
            while (tbKeywords.Text.Contains(",,"))
                tbKeywords.Text = tbKeywords.Text.Replace(",,", ",");
        }

        private void tbYearFor_TextChanged(object sender, EventArgs e)
        {
            tbYearFor.Text = Regex.Replace(tbYearFor.Text, "[^0-9]", "");
        }

        private void tbYearTo_TextChanged(object sender, EventArgs e)
        {
            tbYearTo.Text = Regex.Replace(tbYearTo.Text, "[^0-9]", "");
        }

        private void formSearch_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (checkbYears.Checked
                    && DialogResult != DialogResult.Cancel
                    && int.Parse(tbYearTo.Text) < int.Parse(tbYearFor.Text))
                    throw new Exception();
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка при вводе временного промежутка");
                e.Cancel = true;
                return;
            }

            if (YearFor == "" &&
                Sources == "All" &&
                Keywords.Count == 0 &&
                CheckedCountries.Count == 0 &&
                CheckedUniversities.Count == 0)
                DialogResult = DialogResult.Cancel;
        }
    }
}
