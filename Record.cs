﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace LibStruct
{
    public class Record
    {
        public List<string> authors = new List<string>();
        public List<string> affilations = new List<string>();
        public List<string> countries = new List<string>();
        public List<string> keywords = new List<string>();
        public string DocumentTitle = "";
        public string PublicationTitle = "";
        public string PublicationYear = "";
        public string no = "";
        public string vol = "";
        public string StartPage = "";
        public string EndPage = "";
        public string Abstract = "";
        public string PDF_Link = "";
        public string listOfKeywords = "";
        public string Publisher = "";
        public string DocumentIdentifier = "";
        public string authorsAffilations = "";
        public Record(List<string> rec, List<string> cultureList)
        {
            DocumentTitle = rec[0];
            PublicationTitle = rec[3];
            if (int.TryParse(rec[5], out _))  /// если год имеется
                PublicationYear = rec[5];
            vol = rec[6].Equals("...") ? "-" : rec[6];
            no = rec[7].Equals("...") ? "-" : rec[7];

            DocumentIdentifier = rec[28].ToUpper();
            DocumentIdentifier = DocumentIdentifier.Contains("CONF") ? "Conference" : DocumentIdentifier;
            DocumentIdentifier = DocumentIdentifier.Contains("JOURN") ? "Journal" : DocumentIdentifier;
            DocumentIdentifier = DocumentIdentifier.Contains("MAGAZ") ? "Journal" : DocumentIdentifier;
            if (DocumentIdentifier != "Conference" && DocumentIdentifier != "Journal")
                DocumentIdentifier = "Not defined";

            authors = rec[1].Split(';').Distinct().Select(x => x.Trim()).ToList();
            Publisher = rec[27];
            StartPage = rec[8];
            EndPage = rec[9];
            Abstract = rec[10];
            PDF_Link = rec[15];
            keywords = rec[16].Split(';').Union(rec[17].Split(';')).Distinct().ToList();
            for (int i = 0; i < keywords.Count; i++)
                keywords[i] = keywords[i].ToUpper().Trim();
            keywords.ForEach(kw => listOfKeywords += kw + '\n');
            listOfKeywords = listOfKeywords.Remove(listOfKeywords.Length -1);

            authorsAffilations = rec[2].Replace("; ", "\n").Replace(';', '\n');
            var affilations = rec[2].Split(';');
            foreach (var affilation in affilations)
            {
                var temp = affilation.Split(' ');
                string country = temp[temp.Length - 1].Trim();
                if (int.TryParse(country, out int a) && temp.Length > 1)
                    country = temp[temp.Length - 2].Trim();
                country = initializeCountry(country, cultureList);

                if (!countries.Contains(country))  //  если такой страны ещё не было
                    countries.Add(country);  //  добавим

                temp = affilation.Split(',');
                string university = temp[0].Trim().ToUpper();
                university = string.IsNullOrWhiteSpace(university) ? "Not defined" : university;
                if (!this.affilations.Contains(university))  //  если такого университета ещё не было
                    this.affilations.Add(university);  //  добавим
            }

        }

        private string initializeCountry(string country, List<string> cultureList)
        {
            country = country.ToUpper();
            country = country == "KINGDOM" ? "UNITED KINGDOM" : country;
            country = country.Replace("USA", "UNITED STATES");
            country = country == "UK" ? "UNITED KINGDOM" : country;
            country = country.Replace("U.K.", "UNITED KINGDOM");
            country = country.Replace("ITALIA", "ITALY");
            country = country.Replace("RUSSIAN FEDERATION", "RUSSIA");
            country = Regex.Replace(country, "[^A-Z ]", "");
            if (!cultureList.Contains(country))  //  если страна в файле указана неверно
                country = "Not defined";
            return country;
        }
    }
}
