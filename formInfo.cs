﻿using System.Data;
using System.Windows.Forms;

namespace LibStruct
{
    public partial class formInfo : Form
    {
        public formInfo(Record rec)
        {
            InitializeComponent();
            InitializeDGV(rec);
            Text = "Информация о: " + rec.DocumentTitle;
        }
        void InitializeDGV(Record rec)
        {
            infoDGV.Rows.Clear();
            infoDGV.Columns.Clear();
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Свойства"));
            dt.Columns.Add(new DataColumn("Данные"));
            infoDGV.DataSource = dt;
            infoDGV.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
            infoDGV.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;

            dt.Rows.Add("Заголовок документа", rec.DocumentTitle);
            dt.Rows.Add("Заголовок публикации", rec.PublicationTitle);
            string temp = "";
            foreach (var s in rec.authors)
                temp += s + '\n';
            dt.Rows.Add("Ключевые слова автора", rec.listOfKeywords);
            dt.Rows.Add("Авторы", temp.Substring(0, temp.Length - 1));
            dt.Rows.Add("Учебные заведения авторов", rec.authorsAffilations);
            dt.Rows.Add("Начальная страница", rec.StartPage);
            dt.Rows.Add("Конечная страница", rec.EndPage);
            dt.Rows.Add("Год", rec.PublicationYear);
            dt.Rows.Add("Описание", rec.Abstract);
            dt.Rows.Add("Ссылка на документ", rec.PDF_Link);
            dt.Rows.Add("Опубликовано по инициативе", rec.Publisher);
        }

        private void formInfo_Scroll(object sender, ScrollEventArgs e)
        {
            Update();
        }
    }
}
