﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Xceed.Document.NET;
using Xceed.Words.NET;

namespace LibStruct
{
    public partial class formTables : Form
    {
        SortedDictionary<string, int> years = new SortedDictionary<string, int>();
        List<Record> allowedList;
        formFilter search;
        public formTables()
        {
            allowedList = new List<Record>(formMain.allowedList);
            search = formMain.currentSearh;
            if (search != null && search.YearFor != "" && search.YearTo != "")
                for (int i = int.Parse(search.YearFor); i <= int.Parse(search.YearTo); i++)
                    years.Add(i.ToString(), 0);

            InitializeComponent();
            InitializeYearsDGV();
            InitializeTypesDGV("Conference");
            InitializeCountriesDGV();
            InitializeUniversDGV();
            InitializeKeywordsDGV();
            InitializeCountDGV();
        }

        public formTables(string path, bool useYears, bool useConferences, bool useJournals, bool useNA,
            bool useCountries, bool useUnivers, bool useKeywords, bool useCount, bool isWord)
        {
            allowedList = new List<Record>(formMain.allowedList);
            search = formMain.currentSearh;
            if (search != null && search.YearFor != "" && search.YearTo != "")
                for (int i = int.Parse(search.YearFor); i <= int.Parse(search.YearTo); i++)
                    years.Add(i.ToString(), 0);
            InitializeComponent();
            
            // инициализация выыбраных ранее таблиц
            if (useYears)
                InitializeYearsDGV();
            if (useCountries)
                InitializeCountriesDGV();
            if (useUnivers)
                InitializeUniversDGV();
            if (useKeywords)
                InitializeKeywordsDGV();
            if (useCount)
                InitializeCountDGV();
            // выбор типа документа Excel/Word
            if (isWord)
                WordImportTables(path, useYears, useConferences, useJournals, useNA,
                    useCountries, useUnivers, useKeywords, useCount);
            else
                ExcelImportTables(path, useYears, useConferences, useJournals, useNA,
                useCountries, useUnivers, useKeywords, useCount);
        }
        public void WordImportTables(string path, bool useYears, bool useConferences, bool useJournals, bool useNA,
            bool useCountries, bool useUnivers, bool useKeywords, bool useCount)
        {
            var progress = new formProgress(8);
            progress.Show();

            // создаём документ
            DocX document = DocX.Create(path);
            document.SetDefaultFont(new Font("Times New Roman"), 14, null);

            // заполнение документа таблицами
            if (useYears)
                AddTableInWord(document, yearsDGV, "По годам");
            progress.nextStep();
            if (useConferences)
            {
                InitializeTypesDGV("Conference");
                AddTableInWord(document, typesDGV, "По конференциям");
            }
            progress.nextStep();
            if (useJournals)
            {
                InitializeTypesDGV("Journal");
                AddTableInWord(document, typesDGV, "По журналам");
            }
            progress.nextStep();
            if (useNA)
            {
                InitializeTypesDGV("Not defined");
                AddTableInWord(document, typesDGV, "По неопределённому типу");
            }
            progress.nextStep();
            if (useCountries)
                AddTableInWord(document, countriesDGV, "По странам");
            progress.nextStep();
            if (useUnivers)
                AddTableInWord(document, universDGV, "По учебным заведениям");
            progress.nextStep();
            if (useKeywords)
                AddTableInWord(document, keywordsDGV, "По ключевым словам");
            progress.nextStep();
            if (useCount)
                AddTableInWord(document, countDGV, "По численному составу авторов");
            progress.nextStep();
            progress.Close();

            // добавляем столбцовую диаграмму
            document.InsertChart(CreateBarChart());


            try
            { document.Save(); }
            catch (Exception)
            {
                MessageBox.Show("Проверьте разрешения доступа к указанному пути и антивирус\n" +
                    "Можете попробовать перезапустить программу с правами администратора",
                    "Ошибка сохранения документа",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private static Chart CreateBarChart()
        {
            // создаём столбцовую диаграмму
            BarChart chart = new BarChart();
            // отображаем легенду сверху диаграммы
            chart.AddLegend(ChartLegendPosition.Top, false);

            Series seriesFirst = new Series("Years");
            List<Tuple<string, int>> testDataFirst = new List<Tuple<string, int>>();
            testDataFirst.Add(new Tuple<string, int>("2019", 5));
            testDataFirst.Add(new Tuple<string, int>("2018", 15));
            seriesFirst.Bind(testDataFirst, "Item1", "Item2");

            Series seriesSecond = new Series("Hyears");
            testDataFirst.Clear();
            testDataFirst.Add(new Tuple<string, int>("2019", 30));
            testDataFirst.Add(new Tuple<string, int>("2018", 5));
            seriesSecond.Bind(testDataFirst, "Item1", "Item2");

            chart.AddSeries(seriesFirst);
            chart.AddSeries(seriesSecond);

            return chart;
        }
        void AddTableInWord(DocX document, DataGridView dt, string tableName)
        {
            try
            {
                // продолжим, если таблица не пустая
                if (dt.Rows.Count < 1) return;

                // инициализация новой таблицы
                document.InsertParagraph("Таблица" + '\"' + tableName + '\"').
                         Font("Times New Roman").
                         FontSize(14).
                         Bold();
                Table table = document.AddTable(dt.Rows.Count + 1, dt.Columns.Count);
                table.Design = TableDesign.TableGrid;
                table.Alignment = Alignment.left;

                // заполнение заголовка
                for (int col = 0; col < dt.Columns.Count; col++)
                    table.Rows[0].Cells[col].Paragraphs[0].Append(dt.Columns[col].HeaderText).Bold();
                // заполнение таблицы
                for (int row = 0; row < dt.Rows.Count; row++)
                    for (int col = 0; col < dt.Columns.Count; col++)
                        table.Rows[row + 1].Cells[col].Paragraphs[0].Append(dt.Rows[row].Cells[col].Value.ToString());
                document.InsertParagraph().InsertTableAfterSelf(table);
                document.InsertParagraph().AppendLine();
            }
            catch (Exception _) { }
        }
        public void ExcelImportTables(string path, bool useYears, bool useConferences, bool useJournals, bool useNA,
            bool useCountries, bool useUnivers, bool useKeywords, bool useCount)
        {
            var progress = new formProgress(8);
            progress.Show();
            // инициализация нового документа Excel
            ExcelPackage excelPackage = new ExcelPackage();
            excelPackage.Workbook.Properties.Author = "LibStruct";
            excelPackage.Workbook.Properties.Title = "Импрот " + DateTime.Now.ToString();
            excelPackage.Workbook.Properties.Created = DateTime.Now;

            // заполнение документа таблицами
            if (useYears)
                AddTableInExcel(excelPackage.Workbook, yearsDGV, "По годам");
            progress.nextStep();
            if (useConferences)
            {
                InitializeTypesDGV("Conference");
                AddTableInExcel(excelPackage.Workbook, typesDGV, "По конференциям");
            }
            progress.nextStep();
            if (useJournals)
            {
                InitializeTypesDGV("Journal");
                AddTableInExcel(excelPackage.Workbook, typesDGV, "По журналам");
            }
            progress.nextStep();
            if (useNA)
            {
                InitializeTypesDGV("Not defined");
                AddTableInExcel(excelPackage.Workbook, typesDGV, "По неопределённому типу");
            }
            progress.nextStep();
            if (useCountries)
                AddTableInExcel(excelPackage.Workbook, countriesDGV, "По странам");
            progress.nextStep();
            if (useUnivers)
                AddTableInExcel(excelPackage.Workbook, universDGV, "По учебным заведениям");
            progress.nextStep();
            if (useKeywords)
                AddTableInExcel(excelPackage.Workbook, keywordsDGV, "По ключевым словам");
            progress.nextStep();
            if (useCount)
                AddTableInExcel(excelPackage.Workbook, countDGV, "По численному составу авторов");
            progress.nextStep();

            FileInfo fi = new FileInfo(path);
            excelPackage.SaveAs(fi);
            progress.Close();
        }
        void AddTableInExcel(ExcelWorkbook book, DataGridView dt, string tableName)
        {
            try
            {
                // продолжим, если таблица не пустая
                if (dt.Rows.Count < 1) return;

                // инициализация нового листа книги Excel
                ExcelWorksheet sheet = book.Worksheets.Add(tableName);

                // заполнеие заголовка
                for (int col = 0; col < dt.Columns.Count; col++)
                    sheet.Cells[1, col + 1].Value = dt.Columns[col].HeaderText;
                sheet.Cells[1, 1, 1, dt.Columns.Count].Style.Font.Bold = true;
                
                // выравнивание по центру и установка границ на всю таблицу
                sheet.Cells[1, 1, dt.Rows.Count + 1, dt.Columns.Count].Style.HorizontalAlignment =
                    OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                var border = sheet.Cells[1, 1, dt.Rows.Count + 1, dt.Columns.Count].Style.Border;
                border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;
                border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thick;

                // заполнение таблицы
                for (int row = 0; row < dt.Rows.Count; row++)
                    for (int col = 0; col < dt.Columns.Count; col++)
                        sheet.Cells[row + 2, col + 1].Value = dt.Rows[row].Cells[col].Value.ToString();

                sheet.Cells[1, 1, dt.Rows.Count + 1, dt.Columns.Count].AutoFitColumns();
            }
            catch (Exception) { };
        }
        void InitializeYearsDGV()
        {
            try
            {
                var dt = new System.Data.DataTable();
                dt.Columns.Add(new DataColumn("Год"));
                dt.Columns.Add(new DataColumn("Данные"));
                yearsDGV.DataSource = dt;

                years.Keys.ToList().ForEach(year => years[year] = 0);

                allowedList.ForEach(rec =>
                {
                    if (years.Keys.Contains(rec.PublicationYear))
                        years[rec.PublicationYear]++;
                    else years.Add(rec.PublicationYear, 1);
                });


                foreach (var year in years)
                    dt.Rows.Add(year.Key, year.Value);
            }
            catch (Exception) { tabYears.Parent = null; }
        }

        void InitializeTypesDGV(string type)
        {
            try
            {
                var dt = new System.Data.DataTable();
                dt.Columns.Add(new DataColumn("Год"));
                dt.Columns.Add(new DataColumn("Данные"));
                typesDGV.DataSource = dt;

                years.Keys.ToList().ForEach(year => years[year] = 0);

                allowedList.Where(rec => rec.DocumentIdentifier == type).ToList()
                    .ForEach(rec =>
                    {
                        if (years.Keys.Contains(rec.PublicationYear))
                            years[rec.PublicationYear]++;
                        else
                            years.Add(rec.PublicationYear, 1);
                    });


                foreach (var year in years)
                    dt.Rows.Add(year.Key, year.Value);
                typesDGV.Refresh();
            }
            catch (Exception) { tabTypes.Parent = null; }
        }

        void InitializeCountriesDGV()
        {
            try
            {
                var dt = new System.Data.DataTable();
                dt.Columns.Add(new DataColumn("Год"));
                dt.Columns.Add(new DataColumn("Страна"));
                dt.Columns.Add(new DataColumn("Данные"));
                countriesDGV.DataSource = dt;

                List<string> countries = new List<string>();
                if (search != null && search.CheckedCountries.Count != 0) // если при поиске были выбраны конкретные страны
                    countries = search.CheckedCountries;
                else countries = Properties.Resources.Countries.Split(',').ToList();

                foreach (var country in countries)
                    foreach (var year in years)
                    {
                        int count = 0;
                        foreach (var rec in allowedList)
                            if (rec.PublicationYear.Equals(year.Key) && rec.countries.Contains(country))
                                count++;
                        dt.Rows.Add(year.Key, country, count);
                    }

            }
            catch (Exception) { tabCountries.Parent = null; }
        }

        void InitializeUniversDGV()
        {
            try
            {
                var dt = new System.Data.DataTable();
                dt.Columns.Add(new DataColumn("Год"));
                dt.Columns.Add(new DataColumn("Учебное заведение"));
                dt.Columns.Add(new DataColumn("Данные"));
                universDGV.DataSource = dt;

                List<string> universities = new List<string>();                
                if (search != null && search.CheckedUniversities.Count != 0)
                    universities = search.CheckedUniversities;
                else
                    foreach (var rec in allowedList)
                        universities = universities.Union(rec.affilations).ToList();
                universities = universities.Distinct().ToList();

                foreach (var university in universities)
                    foreach (var year in years)
                    {
                        int count = 0;
                        foreach (var rec in allowedList)
                            if (rec.PublicationYear.Equals(year.Key) && rec.affilations.Contains(university))
                                count++;
                        dt.Rows.Add(year.Key, university, count);
                    }
            }
            catch (Exception) { tabUnivers.Parent = null; }
        }

        void InitializeKeywordsDGV()
        {
            try
            {
                if (search == null || search.Keywords.Count == 0) throw new Exception();
                var dt = new System.Data.DataTable();
                dt.Columns.Add(new DataColumn("Год"));
                dt.Columns.Add(new DataColumn("Ключевое слово"));
                dt.Columns.Add(new DataColumn("Данные"));
                keywordsDGV.DataSource = dt;

                foreach (var kw in search.Keywords)
                {
                    foreach (var year in years)
                    {
                        int count = 0;
                        foreach (var rec in allowedList)
                        {
                            if (!rec.PublicationYear.Equals(year.Key)) continue;
                            bool includeIt = false;
                            foreach (var dataKW in rec.keywords)
                                includeIt = includeIt || dataKW.Contains(kw.ToUpper());
                            if (includeIt)
                                count++;
                        }
                        dt.Rows.Add(year.Key, kw, count);
                    }
                }
            }
            catch (Exception) { tabKeywords.Parent = null; }
        }

        void InitializeCountDGV()
        {
            try
            {
                var dt = new System.Data.DataTable();
                dt.Columns.Add(new DataColumn("Год"));
                dt.Columns.Add(new DataColumn("Количество авторов"));
                countDGV.DataSource = dt;

                List<int> authCounts = new List<int>();
                foreach (var rec in allowedList)
                    if (!authCounts.Contains(rec.authors.Count)) authCounts.Add(rec.authors.Count);

                foreach (var aCount in authCounts)
                    foreach (var year in years)
                    {
                        allowedList.Select(rec => rec.PublicationYear.Equals(year.Key) && rec.authors.Count == aCount);
                        dt.Rows.Add(year.Key, aCount);
                    }
            }
            catch (Exception) { tabCount.Parent = null; }
        }

        private void rbJournals_CheckedChanged(object sender, EventArgs e)
        {
            InitializeTypesDGV("Journal");
        }

        private void rbConferences_CheckedChanged(object sender, EventArgs e)
        {
            InitializeTypesDGV("Conference");
        }
        private void rbNA_CheckedChanged(object sender, EventArgs e)
        {
            InitializeTypesDGV("Not defined");
        }

        private void btImportWord_Click(object sender, EventArgs e)
        {
            using (var form = new formExport(formExport.DocType.Word))
            {
                form.ShowDialog();
                if (form.DialogResult == DialogResult.OK)
                {
                    var t = form.getChecked();
                    WordImportTables(form.getPath(), t[0], t[1], t[2], t[3], t[4], t[5], t[6], t[7]);
                }
            }
        }

        private void btImportExcel_Click(object sender, EventArgs e)
        {
            using (var form = new formExport(formExport.DocType.Excel))
            {
                form.ShowDialog();
                if (form.DialogResult == DialogResult.OK)
                {
                    var t = form.getChecked();
                    ExcelImportTables(form.getPath(), t[0], t[1], t[2], t[3], t[4], t[5], t[6], t[7]);
                }
            }
        }

    }
}
