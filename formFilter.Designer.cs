﻿namespace LibStruct
{
    partial class formFilter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btOk = new System.Windows.Forms.Button();
            this.btCancel = new System.Windows.Forms.Button();
            this.cbSources = new System.Windows.Forms.ComboBox();
            this.lbSources = new System.Windows.Forms.Label();
            this.checkbYears = new System.Windows.Forms.CheckBox();
            this.tbYearTo = new System.Windows.Forms.TextBox();
            this.tbYearFor = new System.Windows.Forms.TextBox();
            this.lbCountries = new System.Windows.Forms.Label();
            this.lbUniversitets = new System.Windows.Forms.Label();
            this.checkListCountries = new System.Windows.Forms.CheckedListBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.checkListUnivers = new System.Windows.Forms.CheckedListBox();
            this.tbKeywords = new System.Windows.Forms.TextBox();
            this.lbKeywords = new System.Windows.Forms.Label();
            this.lbYears = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btOk
            // 
            this.btOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btOk.Location = new System.Drawing.Point(155, 359);
            this.btOk.Name = "btOk";
            this.btOk.Size = new System.Drawing.Size(127, 32);
            this.btOk.TabIndex = 0;
            this.btOk.Text = "Поиск";
            this.btOk.UseVisualStyleBackColor = true;
            // 
            // btCancel
            // 
            this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btCancel.Location = new System.Drawing.Point(383, 359);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(127, 32);
            this.btCancel.TabIndex = 0;
            this.btCancel.Text = "Отмена";
            this.btCancel.UseVisualStyleBackColor = true;
            // 
            // cbSources
            // 
            this.cbSources.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSources.FormattingEnabled = true;
            this.cbSources.Items.AddRange(new object[] {
            "Все источники",
            "Журналы",
            "Конференции"});
            this.cbSources.Location = new System.Drawing.Point(108, 12);
            this.cbSources.Name = "cbSources";
            this.cbSources.Size = new System.Drawing.Size(193, 24);
            this.cbSources.TabIndex = 1;
            // 
            // lbSources
            // 
            this.lbSources.AutoSize = true;
            this.lbSources.Location = new System.Drawing.Point(12, 15);
            this.lbSources.Name = "lbSources";
            this.lbSources.Size = new System.Drawing.Size(79, 17);
            this.lbSources.TabIndex = 2;
            this.lbSources.Text = "Источники";
            // 
            // checkbYears
            // 
            this.checkbYears.AutoSize = true;
            this.checkbYears.Location = new System.Drawing.Point(12, 50);
            this.checkbYears.Name = "checkbYears";
            this.checkbYears.Size = new System.Drawing.Size(90, 21);
            this.checkbYears.TabIndex = 3;
            this.checkbYears.Text = "По годам";
            this.checkbYears.UseVisualStyleBackColor = true;
            this.checkbYears.CheckedChanged += new System.EventHandler(this.cbYears_CheckedChanged);
            // 
            // tbYearTo
            // 
            this.tbYearTo.Location = new System.Drawing.Point(223, 50);
            this.tbYearTo.MaxLength = 4;
            this.tbYearTo.Name = "tbYearTo";
            this.tbYearTo.Size = new System.Drawing.Size(78, 22);
            this.tbYearTo.TabIndex = 4;
            this.tbYearTo.Visible = false;
            this.tbYearTo.TextChanged += new System.EventHandler(this.tbYearTo_TextChanged);
            // 
            // tbYearFor
            // 
            this.tbYearFor.Location = new System.Drawing.Point(108, 50);
            this.tbYearFor.MaxLength = 4;
            this.tbYearFor.Name = "tbYearFor";
            this.tbYearFor.Size = new System.Drawing.Size(76, 22);
            this.tbYearFor.TabIndex = 4;
            this.tbYearFor.Visible = false;
            this.tbYearFor.TextChanged += new System.EventHandler(this.tbYearFor_TextChanged);
            // 
            // lbCountries
            // 
            this.lbCountries.AutoSize = true;
            this.lbCountries.Location = new System.Drawing.Point(319, 15);
            this.lbCountries.Name = "lbCountries";
            this.lbCountries.Size = new System.Drawing.Size(58, 17);
            this.lbCountries.TabIndex = 5;
            this.lbCountries.Text = "Страны";
            // 
            // lbUniversitets
            // 
            this.lbUniversitets.AutoSize = true;
            this.lbUniversitets.Location = new System.Drawing.Point(12, 166);
            this.lbUniversitets.Name = "lbUniversitets";
            this.lbUniversitets.Size = new System.Drawing.Size(141, 17);
            this.lbUniversitets.TabIndex = 6;
            this.lbUniversitets.Text = "Учебные заведения";
            // 
            // checkListCountries
            // 
            this.checkListCountries.CheckOnClick = true;
            this.checkListCountries.FormattingEnabled = true;
            this.checkListCountries.Location = new System.Drawing.Point(383, 15);
            this.checkListCountries.Name = "checkListCountries";
            this.checkListCountries.Size = new System.Drawing.Size(282, 140);
            this.checkListCountries.TabIndex = 9;
            this.checkListCountries.SelectedIndexChanged += new System.EventHandler(this.checkListCountries_SelectedIndexChanged);
            // 
            // checkListUnivers
            // 
            this.checkListUnivers.CheckOnClick = true;
            this.checkListUnivers.FormattingEnabled = true;
            this.checkListUnivers.Location = new System.Drawing.Point(12, 186);
            this.checkListUnivers.Name = "checkListUnivers";
            this.checkListUnivers.Size = new System.Drawing.Size(653, 157);
            this.checkListUnivers.TabIndex = 9;
            this.checkListUnivers.SelectedIndexChanged += new System.EventHandler(this.checkListUnivers_SelectedIndexChanged);
            // 
            // tbKeywords
            // 
            this.tbKeywords.Location = new System.Drawing.Point(12, 106);
            this.tbKeywords.MaxLength = 70;
            this.tbKeywords.Multiline = true;
            this.tbKeywords.Name = "tbKeywords";
            this.tbKeywords.Size = new System.Drawing.Size(289, 49);
            this.tbKeywords.TabIndex = 11;
            this.tbKeywords.TextChanged += new System.EventHandler(this.tbKeywords_TextChanged);
            // 
            // lbKeywords
            // 
            this.lbKeywords.AutoSize = true;
            this.lbKeywords.Location = new System.Drawing.Point(9, 84);
            this.lbKeywords.Name = "lbKeywords";
            this.lbKeywords.Size = new System.Drawing.Size(230, 17);
            this.lbKeywords.TabIndex = 10;
            this.lbKeywords.Text = "Ключевые слова (через запятую)";
            // 
            // lbYears
            // 
            this.lbYears.AutoSize = true;
            this.lbYears.Location = new System.Drawing.Point(196, 51);
            this.lbYears.Name = "lbYears";
            this.lbYears.Size = new System.Drawing.Size(16, 17);
            this.lbYears.TabIndex = 5;
            this.lbYears.Text = "—";
            this.lbYears.Visible = false;
            // 
            // formFilter
            // 
            this.AcceptButton = this.btOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Azure;
            this.CancelButton = this.btCancel;
            this.ClientSize = new System.Drawing.Size(677, 403);
            this.Controls.Add(this.tbKeywords);
            this.Controls.Add(this.lbKeywords);
            this.Controls.Add(this.checkListUnivers);
            this.Controls.Add(this.checkListCountries);
            this.Controls.Add(this.lbUniversitets);
            this.Controls.Add(this.lbYears);
            this.Controls.Add(this.lbCountries);
            this.Controls.Add(this.tbYearFor);
            this.Controls.Add(this.tbYearTo);
            this.Controls.Add(this.checkbYears);
            this.Controls.Add(this.lbSources);
            this.Controls.Add(this.cbSources);
            this.Controls.Add(this.btCancel);
            this.Controls.Add(this.btOk);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "formFilter";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Задать критерии фильтрации";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.formSearch_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btOk;
        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.ComboBox cbSources;
        private System.Windows.Forms.Label lbSources;
        private System.Windows.Forms.CheckBox checkbYears;
        private System.Windows.Forms.TextBox tbYearTo;
        private System.Windows.Forms.TextBox tbYearFor;
        private System.Windows.Forms.Label lbCountries;
        private System.Windows.Forms.Label lbUniversitets;
        private System.Windows.Forms.CheckedListBox checkListCountries;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.CheckedListBox checkListUnivers;
        private System.Windows.Forms.TextBox tbKeywords;
        private System.Windows.Forms.Label lbKeywords;
        private System.Windows.Forms.Label lbYears;
    }
}