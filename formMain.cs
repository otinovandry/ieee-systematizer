﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Xceed.Document.NET;
using Xceed.Words.NET;

namespace LibStruct
{
    public partial class formMain : Form
    {
        public static Dictionary<string, Record> Data = new Dictionary<string, Record>(); // "чистые" данные, только нужные поля
        public static List<Record> allowedList = new List<Record>();
        public static formFilter currentSearh;
        private DataTable dt = new DataTable();
        public formMain()
        {
            InitializeComponent();
            dt.Columns.Add(new DataColumn("Название публикации"));
            mainDGV.DataSource = dt;
        }

        public static List<string> Parser(string line, string[] separator)
        {
            line = ',' + line + ',';
            while (line.Contains(",\"\","))  // пока есть пустые необработанные поля
                line = line.Replace(",\"\",", ",\"...\",");  //  заменям пустоту на ...
            line = line.Substring(2, line.Length - 4);  // убираем лишние символы по краям
            var fields = (line.Split(separator, StringSplitOptions.RemoveEmptyEntries)).ToList();
            return fields;
        }

        void RemoveAllData()
        {
            Data.Clear();
            allowedList.Clear();
            dt.Clear();
        }

        string GenerateUnicHeader(string name)
        {
            int i = 0;
            var temp = name;
            while (Data.ContainsKey(temp))
                temp = name + '(' + (++i).ToString() + ')';
            return temp;
        }

        void EnableDataCotrols(bool val)
        {
            импортВWordToolStripMenuItem.Enabled =
                импортВExcelToolStripMenuItem.Enabled =
                labelRowsInfo.Visible =
                labelRowsCount.Visible =
                расширенныйПоискToolStripMenuItem.Visible =
                диаграммаToolStripMenuItem.Visible =
                таблицыToolStripMenuItem.Visible = val;
        }

        void Read_CSV(string path)
        {
            RemoveAllData();
            using (var sr = new StreamReader(path))  // начинаем чтение
            {
                // импортируем список всех стран
                var cultureList = Properties.Resources.Countries.Split(',').ToList();
                cultureList.Sort();
                string heads = sr.ReadLine().Replace("\"", "");  //  читаем первую строку заголовков и удаляем кавычки
                var headers = (heads.Split(',')).ToList();
                string errorLine = "";  //  список некорректных строк

                string[] separator = { "\",\"" }; //  разделитель полей 
                while (!sr.EndOfStream)  //  считываем и парсим остальные строки
                {
                    string line = sr.ReadLine();
                    List<string> fields = Parser(line, separator);

                    if (fields.Count != headers.Count)  // если число полей в текущей строке некорректно
                    {
                        errorLine += "Строка: \"" + fields[0] + " ..." + "\" Оказалась некорректной\n";
                        continue;
                    }
                    string name = fields[0];
                    name = GenerateUnicHeader(name);
                    Data.Add(name, new Record(fields, cultureList));
                    dt.Rows.Add(name);
                }
                if (errorLine != "")
                    MessageBox.Show(errorLine);
            }

            if (Data.Count < 1)
                throw new Exception(); // если файл пустой
            else
                allowedList = Data.Values.ToList();  // разрешенный список - все значения
        }

        private void открытьНовыйToolStripMenuItem_Click(object sender, EventArgs e)
        {
            сброситьПоискToolStripMenuItem1_Click(sender, e);
            var dlg = new OpenFileDialog();
            dlg.Filter = "CSV-таблица (*.csv)|*.csv";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    Read_CSV(dlg.FileName); //  считываем данные из выбранного файла
                    labelRowsCount.Text = dt.Rows.Count.ToString();
                    // включаем отображение управляющих элементов
                    EnableDataCotrols(true);
                }
                catch (Exception)
                {
                    RemoveAllData();
                    EnableDataCotrols(false);
                    MessageBox.Show("Ошибка чтения файла, проверьте его корректность");
                }
            }
        }

        private void mainDGV_Scroll(object sender, ScrollEventArgs e)
        {
            mainDGV.Update();
        }

        private void dsjlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void mainDGV_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == -1 || e.RowIndex == -1) return;
            // открытие подробной информации выбранной публикации
            var name = mainDGV[e.ColumnIndex, e.RowIndex].Value.ToString();
            var infoForm = new formInfo(Data[name]);
            infoForm.Show();
        }

        private void MainForm_ResizeBegin(object sender, EventArgs e)
        {
            fakePanel.Visible = true;  // оптимизация отрисовки графики
            mainDGV.DataSource = null;
        }

        private void MainForm_ResizeEnd(object sender, EventArgs e)
        {
            fakePanel.Visible = false;
            mainDGV.DataSource = dt;
        }

        private void диаграммаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new formCharts().Show();
        }

        private void сброситьПоискToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            расширенныйПоискToolStripMenuItem.Enabled = true;
            mainDGV.DataSource = dt;
            labelRowsCount.Text = dt.Rows.Count.ToString();
            allowedList = Data.Values.ToList();
            сброситьПоискToolStripMenuItem1.Visible = false;
            currentSearh = null;
        }

        private void расширенныйПоискToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var search = new formFilter(Data))
            {
                if (search.ShowDialog() == DialogResult.OK)  // если поиск был применён
                {
                    расширенныйПоискToolStripMenuItem.Enabled = false;
                    currentSearh = search;
                    сброситьПоискToolStripMenuItem1.Visible = true;
                    DataTable tempDT = new DataTable();
                    tempDT.Columns.Add(new DataColumn("Название публикации"));

                    allowedList.Clear();
                    foreach (var rec in Data)
                    {
                        if (search.YearFor != "" && search.YearFor != "" &&  //  если поля годов имеются
                            int.TryParse(rec.Value.PublicationYear, out int year))  // проверим на год
                        {
                            if (year < int.Parse(search.YearFor) || year > int.Parse(search.YearTo))
                                continue;
                        }

                        if (search.CheckedCountries.Count != 0)  // если список отмеченных стран не пуст
                        {
                            if (!rec.Value.countries.Any(country => search.CheckedCountries.Contains(country))) //  если публикация не имеет страну из списка
                                continue;
                        }

                        if (search.CheckedUniversities.Count != 0)  // если список отмеченных учебных заведений не пуст
                        {
                            if (!rec.Value.affilations.Any(aff => search.CheckedUniversities.Contains(aff))) //  если публикация не имеет учебное заведение из списка
                                continue;
                        }

                        if (search.Keywords.Count != 0)  // если список ключевых слов не пуст
                        {
                            bool includeIt = false;
                            foreach (var kw in search.Keywords)
                            {
                                includeIt = false;
                                foreach (var dataKW in rec.Value.keywords)
                                    includeIt = includeIt || dataKW.Equals(kw, StringComparison.InvariantCultureIgnoreCase);
                            }
                            if (!includeIt)
                                continue;
                        }

                        if (search.Sources != rec.Value.DocumentIdentifier && search.Sources != "All")
                            continue;

                        allowedList.Add(rec.Value);
                    }

                    foreach (var record in allowedList)
                        tempDT.Rows.Add(record.DocumentTitle);
                    mainDGV.DataSource = tempDT;
                    labelRowsCount.Text = tempDT.Rows.Count.ToString();
                }
            }
        }

        private void таблицыToolStripMenuItem_Click(object sender, EventArgs e) => new formTables().Show();

        private void импортВExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var import = new formExport(formExport.DocType.Excel))
            {
                import.ShowDialog();
                if (import.DialogResult == DialogResult.OK)
                {
                    var t = import.getChecked(); // список выборов
                                                 // создание нового документа Excel
                    new formTables(import.getPath(), t[0], t[1], t[2], t[3], t[4], t[5], t[6], t[7], false);
                }
            }
        }

        private void диаграммыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var import = new formExport(formExport.DocType.Word))
            {
                import.ShowDialog();
                if (import.DialogResult == DialogResult.OK)
                {
                    var t = import.getChecked(); // список выборов
                                                 // создание нового документа Word
                    new formCharts(import.getPath(), t[0], t[1], t[2], t[3], t[4], t[5], t[6], t[7], true);
                }
            }
        }

        private void записи_IEEE_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var dlg = new SaveFileDialog())
            {
                dlg.Filter = "Документ MS Word (*.docx)|*.docx";
                if (dlg.ShowDialog() == DialogResult.OK && allowedList.Count > 0)
                {
                    var reductions = new List<Tuple<string, string>>();  //  список сокращений стандарта IEEE
                    foreach (var x in Properties.Resources.Reductions.Split('\n'))
                        reductions.Add(new Tuple<string, string>(x.Split(' ')[0], x.Split(' ')[1].Replace("\r", "")));

                    DocX document = DocX.Create(dlg.FileName); // создаем документ Word
                    document.SetDefaultFont(new Font("Times New Roman"), 14, null);
                    int num = 0;
                    foreach (var rec in allowedList)
                    { // проходим по всем записям
                        var paragraph = document.InsertParagraph();
                        paragraph.IndentationHanging = 20;
                        paragraph.Append("[" + (++num).ToString() + "] ");
                        foreach (var author in rec.authors)
                            paragraph.Append(author + ", ");
                        paragraph.Append("\"" + redStr(reductions, rec.DocumentTitle) + "\", ");
                        paragraph.Append(redStr(reductions, rec.PublicationTitle)).Italic();
                        paragraph.Append(", ");
                        if (rec.DocumentIdentifier == "Conference")
                        { // вывод конференции
                            paragraph.Append(rec.PublicationYear);
                            if (int.TryParse(rec.StartPage, out int a) && int.TryParse(rec.EndPage, out int b))
                                paragraph.Append(", pp. " + rec.StartPage + '-' + rec.EndPage + '.');
                            else
                                paragraph.Append(".");
                        }
                        else // вывод журнала
                        {
                            if (int.TryParse(rec.vol, out int a) && int.TryParse(rec.no, out int b))
                            {
                                paragraph.Append("vol. " + rec.vol + ", ");
                                paragraph.Append("no. " + rec.no + ", ");
                            }
                            if (int.TryParse(rec.StartPage, out int c) && int.TryParse(rec.EndPage, out int d))
                                paragraph.Append("pp. " + rec.StartPage + '-' + rec.EndPage + ", ");
                            paragraph.Append(rec.PublicationYear + '.');
                        }
                        paragraph.AppendLine();
                    }

                    try
                    { document.Save(); }
                    catch (Exception)
                    {
                        MessageBox.Show("Проверьте разрешения доступа к указанному пути и антивирус\n" +
                            "Можете попробовать перезапустить программу с правами администратора",
                            "Ошибка сохранения документа",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                    }
                }
            }
        }

        private string redStr(List<Tuple<string, string>> reductions, string str) // вводит сокращения для стандарта IEEE
        {
            foreach (var x in reductions)
            {
                while (str.Contains(x.Item1))
                    str = str.Replace(x.Item1, x.Item2);
            }
            return str;
        }
        private void таблицыToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            using (var import = new formExport(formExport.DocType.Word))
            {
                import.ShowDialog();
                if (import.DialogResult == DialogResult.OK)
                {
                    var t = import.getChecked(); // список выборов
                                                 // создание нового документа Word
                    new formTables(import.getPath(), t[0], t[1], t[2], t[3], t[4], t[5], t[6], t[7], true);
                }
            }
        }

        private void записиПоСтандартуГОСТToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var dlg = new SaveFileDialog())
            {
                dlg.Filter = "Документ MS Word (*.docx)|*.docx";
                if (dlg.ShowDialog() == DialogResult.OK && allowedList.Count > 0)
                {
                    DocX document = DocX.Create(dlg.FileName); // создаем документ Word
                    document.SetDefaultFont(new Font("Times New Roman"), 14, null);
                    int num = 0;
                    foreach (var rec in allowedList)
                    { // проходим по всем записям
                        var paragraph = document.InsertParagraph();
                        paragraph.IndentationHanging = 15;
                        paragraph.Append((++num).ToString() + ". ");
                        if (rec.DocumentIdentifier == "Conference")
                        {
                            foreach (var author in rec.authors)
                                if (!author.Equals(rec.authors.Last()))
                                    paragraph.Append(author + ", ");
                                else
                                    paragraph.Append(author);
                            paragraph.Append(" " + rec.DocumentTitle + ". ");
                            paragraph.Append(rec.PublicationTitle + ",").Italic();
                            paragraph.Append(rec.Publisher + ", " + rec.PublicationYear + ", vol" + rec.vol);
                            if (int.TryParse(rec.StartPage, out int c) && int.TryParse(rec.EndPage, out int d))
                                paragraph.Append(", pp. " + rec.StartPage + '-' + rec.EndPage);
                        }
                        else
                        {
                            if (rec.authors.Count < 4)
                            {
                                foreach (var author in rec.authors)
                                    if (!author.Equals(rec.authors.Last()))
                                        paragraph.Append(author + ", ");
                                    else
                                        paragraph.Append(author + " ");
                                paragraph.Append(rec.DocumentTitle);
                            }
                            else
                            {
                                paragraph.Append(rec.DocumentTitle + " / ");
                                foreach (var author in rec.authors)
                                    if (!author.Equals(rec.authors.Last()))
                                        paragraph.Append(author + ", ");
                                    else
                                        paragraph.Append(author);
                            }
                            paragraph.Append(" // " + rec.PublicationTitle + ". " + rec.PublicationYear + ". № " + rec.no);
                            if (int.TryParse(rec.StartPage, out int c) && int.TryParse(rec.EndPage, out int d))
                                paragraph.Append(" P. " + rec.StartPage + '-' + rec.EndPage);
                        }
                    }
                    try
                    { document.Save(); }
                    catch (Exception)
                    {
                        MessageBox.Show("Проверьте разрешения доступа к указанному пути и антивирус\n" +
                            "Можете попробовать перезапустить программу с правами администратора",
                            "Ошибка сохранения документа",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                    }
                }
            }
        }
    }
}