﻿namespace LibStruct
{
    partial class formExport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btCancel = new System.Windows.Forms.Button();
            this.checkedLB = new System.Windows.Forms.CheckedListBox();
            this.btOK = new System.Windows.Forms.Button();
            this.cbCheckAll = new System.Windows.Forms.CheckBox();
            this.tbPath = new System.Windows.Forms.TextBox();
            this.btBrowse = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btCancel
            // 
            this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btCancel.Location = new System.Drawing.Point(254, 271);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(128, 51);
            this.btCancel.TabIndex = 0;
            this.btCancel.Text = "Отмена";
            this.btCancel.UseVisualStyleBackColor = true;
            this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
            // 
            // checkedLB
            // 
            this.checkedLB.BackColor = System.Drawing.Color.Azure;
            this.checkedLB.CheckOnClick = true;
            this.checkedLB.FormattingEnabled = true;
            this.checkedLB.Items.AddRange(new object[] {
            "По годам",
            "По конференциям и годам",
            "По журналам и годам",
            "По неуказанным типам и годам",
            "По странам и годам",
            "По учебным заведениям и годам",
            "По ключевым словам и годам",
            "По количественному составу авторов и годам"});
            this.checkedLB.Location = new System.Drawing.Point(13, 13);
            this.checkedLB.Name = "checkedLB";
            this.checkedLB.Size = new System.Drawing.Size(369, 140);
            this.checkedLB.TabIndex = 1;
            // 
            // btOK
            // 
            this.btOK.Location = new System.Drawing.Point(13, 271);
            this.btOK.Name = "btOK";
            this.btOK.Size = new System.Drawing.Size(128, 51);
            this.btOK.TabIndex = 2;
            this.btOK.Text = "Экспорт";
            this.btOK.UseVisualStyleBackColor = true;
            this.btOK.Click += new System.EventHandler(this.btOK_Click);
            // 
            // cbCheckAll
            // 
            this.cbCheckAll.AutoSize = true;
            this.cbCheckAll.Location = new System.Drawing.Point(13, 159);
            this.cbCheckAll.Name = "cbCheckAll";
            this.cbCheckAll.Size = new System.Drawing.Size(113, 21);
            this.cbCheckAll.TabIndex = 3;
            this.cbCheckAll.Text = "Выбрать всё";
            this.cbCheckAll.UseVisualStyleBackColor = true;
            this.cbCheckAll.CheckedChanged += new System.EventHandler(this.cbCheckAll_CheckedChanged);
            // 
            // tbPath
            // 
            this.tbPath.Location = new System.Drawing.Point(17, 212);
            this.tbPath.Name = "tbPath";
            this.tbPath.ReadOnly = true;
            this.tbPath.Size = new System.Drawing.Size(365, 22);
            this.tbPath.TabIndex = 4;
            // 
            // btBrowse
            // 
            this.btBrowse.Location = new System.Drawing.Point(303, 240);
            this.btBrowse.Name = "btBrowse";
            this.btBrowse.Size = new System.Drawing.Size(79, 25);
            this.btBrowse.TabIndex = 5;
            this.btBrowse.Text = "Обзор...";
            this.btBrowse.UseVisualStyleBackColor = true;
            this.btBrowse.Click += new System.EventHandler(this.btBrowse_Click);
            // 
            // formExport
            // 
            this.AcceptButton = this.btOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btCancel;
            this.ClientSize = new System.Drawing.Size(394, 334);
            this.Controls.Add(this.btBrowse);
            this.Controls.Add(this.tbPath);
            this.Controls.Add(this.cbCheckAll);
            this.Controls.Add(this.btOK);
            this.Controls.Add(this.checkedLB);
            this.Controls.Add(this.btCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "formExport";
            this.Text = "Экспорт";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.CheckedListBox checkedLB;
        private System.Windows.Forms.Button btOK;
        private System.Windows.Forms.CheckBox cbCheckAll;
        private System.Windows.Forms.TextBox tbPath;
        private System.Windows.Forms.Button btBrowse;
    }
}