﻿namespace LibStruct
{
    partial class formCharts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabYears = new System.Windows.Forms.TabPage();
            this.rbPie = new System.Windows.Forms.RadioButton();
            this.rbColumn = new System.Windows.Forms.RadioButton();
            this.chartForYears = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabTypes = new System.Windows.Forms.TabPage();
            this.rbNA = new System.Windows.Forms.RadioButton();
            this.rbConferences = new System.Windows.Forms.RadioButton();
            this.rbJournals = new System.Windows.Forms.RadioButton();
            this.chartForTypes = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabCountries = new System.Windows.Forms.TabPage();
            this.chartForCountries = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabUnivers = new System.Windows.Forms.TabPage();
            this.chartForAffilations = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabKeywords = new System.Windows.Forms.TabPage();
            this.chartForKeywords = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabCount = new System.Windows.Forms.TabPage();
            this.chartForCount = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabControl1.SuspendLayout();
            this.tabYears.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartForYears)).BeginInit();
            this.tabTypes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartForTypes)).BeginInit();
            this.tabCountries.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartForCountries)).BeginInit();
            this.tabUnivers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartForAffilations)).BeginInit();
            this.tabKeywords.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartForKeywords)).BeginInit();
            this.tabCount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartForCount)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabYears);
            this.tabControl1.Controls.Add(this.tabTypes);
            this.tabControl1.Controls.Add(this.tabCountries);
            this.tabControl1.Controls.Add(this.tabUnivers);
            this.tabControl1.Controls.Add(this.tabKeywords);
            this.tabControl1.Controls.Add(this.tabCount);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.MinimumSize = new System.Drawing.Size(1033, 581);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1033, 581);
            this.tabControl1.TabIndex = 1;
            // 
            // tabYears
            // 
            this.tabYears.Controls.Add(this.rbPie);
            this.tabYears.Controls.Add(this.rbColumn);
            this.tabYears.Controls.Add(this.chartForYears);
            this.tabYears.Location = new System.Drawing.Point(4, 25);
            this.tabYears.Name = "tabYears";
            this.tabYears.Padding = new System.Windows.Forms.Padding(3);
            this.tabYears.Size = new System.Drawing.Size(1025, 552);
            this.tabYears.TabIndex = 1;
            this.tabYears.Text = "По годам(все)";
            this.tabYears.UseVisualStyleBackColor = true;
            // 
            // rbPie
            // 
            this.rbPie.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rbPie.AutoSize = true;
            this.rbPie.Checked = true;
            this.rbPie.Location = new System.Drawing.Point(6, 525);
            this.rbPie.Name = "rbPie";
            this.rbPie.Size = new System.Drawing.Size(89, 21);
            this.rbPie.TabIndex = 4;
            this.rbPie.TabStop = true;
            this.rbPie.Text = "Круговая";
            this.rbPie.UseVisualStyleBackColor = true;
            this.rbPie.CheckedChanged += new System.EventHandler(this.rbPie_CheckedChanged);
            // 
            // rbColumn
            // 
            this.rbColumn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rbColumn.AutoSize = true;
            this.rbColumn.Location = new System.Drawing.Point(101, 525);
            this.rbColumn.Name = "rbColumn";
            this.rbColumn.Size = new System.Drawing.Size(115, 21);
            this.rbColumn.TabIndex = 4;
            this.rbColumn.Text = "Ступенчатая";
            this.rbColumn.UseVisualStyleBackColor = true;
            this.rbColumn.CheckedChanged += new System.EventHandler(this.rbColumn_CheckedChanged);
            // 
            // chartForYears
            // 
            this.chartForYears.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea1.Name = "ChartArea1";
            this.chartForYears.ChartAreas.Add(chartArea1);
            this.chartForYears.Location = new System.Drawing.Point(1, 1);
            this.chartForYears.Name = "chartForYears";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chartForYears.Series.Add(series1);
            this.chartForYears.Size = new System.Drawing.Size(1022, 549);
            this.chartForYears.TabIndex = 2;
            this.chartForYears.Text = "chartForYears";
            // 
            // tabTypes
            // 
            this.tabTypes.Controls.Add(this.rbNA);
            this.tabTypes.Controls.Add(this.rbConferences);
            this.tabTypes.Controls.Add(this.rbJournals);
            this.tabTypes.Controls.Add(this.chartForTypes);
            this.tabTypes.Location = new System.Drawing.Point(4, 25);
            this.tabTypes.Name = "tabTypes";
            this.tabTypes.Padding = new System.Windows.Forms.Padding(3);
            this.tabTypes.Size = new System.Drawing.Size(1025, 552);
            this.tabTypes.TabIndex = 2;
            this.tabTypes.Text = "По годам и типам";
            this.tabTypes.UseVisualStyleBackColor = true;
            // 
            // rbNA
            // 
            this.rbNA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rbNA.AutoSize = true;
            this.rbNA.Location = new System.Drawing.Point(232, 528);
            this.rbNA.Name = "rbNA";
            this.rbNA.Size = new System.Drawing.Size(123, 21);
            this.rbNA.TabIndex = 6;
            this.rbNA.Text = "Тип не указан";
            this.rbNA.UseVisualStyleBackColor = true;
            this.rbNA.CheckedChanged += new System.EventHandler(this.rbNA_CheckedChanged);
            // 
            // rbConferences
            // 
            this.rbConferences.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rbConferences.AutoSize = true;
            this.rbConferences.Checked = true;
            this.rbConferences.Location = new System.Drawing.Point(8, 528);
            this.rbConferences.Name = "rbConferences";
            this.rbConferences.Size = new System.Drawing.Size(121, 21);
            this.rbConferences.TabIndex = 5;
            this.rbConferences.TabStop = true;
            this.rbConferences.Text = "Конференции";
            this.rbConferences.UseVisualStyleBackColor = true;
            this.rbConferences.CheckedChanged += new System.EventHandler(this.rbConferences_CheckedChanged);
            // 
            // rbJournals
            // 
            this.rbJournals.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rbJournals.AutoSize = true;
            this.rbJournals.Location = new System.Drawing.Point(135, 528);
            this.rbJournals.Name = "rbJournals";
            this.rbJournals.Size = new System.Drawing.Size(91, 21);
            this.rbJournals.TabIndex = 5;
            this.rbJournals.Text = "Журналы";
            this.rbJournals.UseVisualStyleBackColor = true;
            this.rbJournals.CheckedChanged += new System.EventHandler(this.rbJournals_CheckedChanged);
            // 
            // chartForTypes
            // 
            this.chartForTypes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea2.Name = "ChartArea1";
            this.chartForTypes.ChartAreas.Add(chartArea2);
            this.chartForTypes.Location = new System.Drawing.Point(1, 1);
            this.chartForTypes.Name = "chartForTypes";
            series2.ChartArea = "ChartArea1";
            series2.Name = "Series1";
            this.chartForTypes.Series.Add(series2);
            this.chartForTypes.Size = new System.Drawing.Size(1020, 549);
            this.chartForTypes.TabIndex = 3;
            this.chartForTypes.Text = "chartForTypes";
            // 
            // tabCountries
            // 
            this.tabCountries.Controls.Add(this.chartForCountries);
            this.tabCountries.Location = new System.Drawing.Point(4, 25);
            this.tabCountries.Name = "tabCountries";
            this.tabCountries.Padding = new System.Windows.Forms.Padding(3);
            this.tabCountries.Size = new System.Drawing.Size(1025, 552);
            this.tabCountries.TabIndex = 3;
            this.tabCountries.Text = "По странам и годам";
            this.tabCountries.UseVisualStyleBackColor = true;
            // 
            // chartForCountries
            // 
            this.chartForCountries.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea3.Name = "ChartArea1";
            this.chartForCountries.ChartAreas.Add(chartArea3);
            this.chartForCountries.Location = new System.Drawing.Point(2, 2);
            this.chartForCountries.Name = "chartForCountries";
            series3.ChartArea = "ChartArea1";
            series3.Name = "Series1";
            this.chartForCountries.Series.Add(series3);
            this.chartForCountries.Size = new System.Drawing.Size(1020, 549);
            this.chartForCountries.TabIndex = 4;
            this.chartForCountries.Text = "Countries";
            // 
            // tabUnivers
            // 
            this.tabUnivers.Controls.Add(this.chartForAffilations);
            this.tabUnivers.Location = new System.Drawing.Point(4, 25);
            this.tabUnivers.Name = "tabUnivers";
            this.tabUnivers.Size = new System.Drawing.Size(1025, 552);
            this.tabUnivers.TabIndex = 4;
            this.tabUnivers.Text = "По годам и уч. заведениям";
            this.tabUnivers.UseVisualStyleBackColor = true;
            // 
            // chartForAffilations
            // 
            this.chartForAffilations.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea4.Name = "ChartArea1";
            this.chartForAffilations.ChartAreas.Add(chartArea4);
            this.chartForAffilations.Location = new System.Drawing.Point(2, 2);
            this.chartForAffilations.Name = "chartForAffilations";
            series4.ChartArea = "ChartArea1";
            series4.Name = "Series1";
            this.chartForAffilations.Series.Add(series4);
            this.chartForAffilations.Size = new System.Drawing.Size(1020, 549);
            this.chartForAffilations.TabIndex = 5;
            this.chartForAffilations.Text = "Countries";
            // 
            // tabKeywords
            // 
            this.tabKeywords.Controls.Add(this.chartForKeywords);
            this.tabKeywords.Location = new System.Drawing.Point(4, 25);
            this.tabKeywords.Name = "tabKeywords";
            this.tabKeywords.Size = new System.Drawing.Size(1025, 552);
            this.tabKeywords.TabIndex = 5;
            this.tabKeywords.Text = "По годам и клюевым словам";
            this.tabKeywords.UseVisualStyleBackColor = true;
            // 
            // chartForKeywords
            // 
            this.chartForKeywords.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea5.Name = "ChartArea1";
            this.chartForKeywords.ChartAreas.Add(chartArea5);
            this.chartForKeywords.Location = new System.Drawing.Point(2, 2);
            this.chartForKeywords.Name = "chartForKeywords";
            series5.ChartArea = "ChartArea1";
            series5.Name = "Series1";
            this.chartForKeywords.Series.Add(series5);
            this.chartForKeywords.Size = new System.Drawing.Size(1020, 549);
            this.chartForKeywords.TabIndex = 6;
            this.chartForKeywords.Text = "Countries";
            // 
            // tabCount
            // 
            this.tabCount.Controls.Add(this.chartForCount);
            this.tabCount.Location = new System.Drawing.Point(4, 25);
            this.tabCount.Name = "tabCount";
            this.tabCount.Size = new System.Drawing.Size(1025, 552);
            this.tabCount.TabIndex = 6;
            this.tabCount.Text = "По годам и кол-ву авторов";
            this.tabCount.UseVisualStyleBackColor = true;
            // 
            // chartForCount
            // 
            this.chartForCount.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea6.Name = "ChartArea1";
            this.chartForCount.ChartAreas.Add(chartArea6);
            this.chartForCount.Location = new System.Drawing.Point(1, 2);
            this.chartForCount.Name = "chartForCount";
            series6.ChartArea = "ChartArea1";
            series6.Name = "Series1";
            this.chartForCount.Series.Add(series6);
            this.chartForCount.Size = new System.Drawing.Size(1022, 549);
            this.chartForCount.TabIndex = 3;
            this.chartForCount.Text = "chart1";
            // 
            // formCharts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1030, 573);
            this.Controls.Add(this.tabControl1);
            this.MinimumSize = new System.Drawing.Size(1030, 620);
            this.Name = "formCharts";
            this.Text = "Диаграммы";
            this.tabControl1.ResumeLayout(false);
            this.tabYears.ResumeLayout(false);
            this.tabYears.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartForYears)).EndInit();
            this.tabTypes.ResumeLayout(false);
            this.tabTypes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartForTypes)).EndInit();
            this.tabCountries.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartForCountries)).EndInit();
            this.tabUnivers.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartForAffilations)).EndInit();
            this.tabKeywords.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartForKeywords)).EndInit();
            this.tabCount.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartForCount)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabYears;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartForYears;
        private System.Windows.Forms.TabPage tabTypes;
        private System.Windows.Forms.RadioButton rbPie;
        private System.Windows.Forms.RadioButton rbColumn;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartForTypes;
        private System.Windows.Forms.RadioButton rbConferences;
        private System.Windows.Forms.RadioButton rbJournals;
        private System.Windows.Forms.TabPage tabCountries;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartForCountries;
        private System.Windows.Forms.TabPage tabUnivers;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartForAffilations;
        private System.Windows.Forms.TabPage tabKeywords;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartForKeywords;
        private System.Windows.Forms.TabPage tabCount;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartForCount;
        private System.Windows.Forms.RadioButton rbNA;
    }
}