﻿namespace LibStruct
{
    partial class formMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.открытьНовыйToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.импортВExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.импортВWordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.таблицыToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ткущиеЗаписиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.записиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.записиПоСтандартуГОСТToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dsjlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.расширенныйПоискToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сброситьПоискToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.диаграммаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.таблицыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fakePanel = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.mainDGV = new System.Windows.Forms.DataGridView();
            this.labelRowsCount = new System.Windows.Forms.Label();
            this.labelRowsInfo = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.fakePanel.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.открытьНовыйToolStripMenuItem,
            this.импортВExcelToolStripMenuItem,
            this.импортВWordToolStripMenuItem,
            this.dsjlToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(59, 24);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // открытьНовыйToolStripMenuItem
            // 
            this.открытьНовыйToolStripMenuItem.Name = "открытьНовыйToolStripMenuItem";
            this.открытьНовыйToolStripMenuItem.Size = new System.Drawing.Size(250, 26);
            this.открытьНовыйToolStripMenuItem.Text = "Открыть новый...";
            this.открытьНовыйToolStripMenuItem.Click += new System.EventHandler(this.открытьНовыйToolStripMenuItem_Click);
            // 
            // импортВExcelToolStripMenuItem
            // 
            this.импортВExcelToolStripMenuItem.Enabled = false;
            this.импортВExcelToolStripMenuItem.Name = "импортВExcelToolStripMenuItem";
            this.импортВExcelToolStripMenuItem.Size = new System.Drawing.Size(247, 26);
            this.импортВExcelToolStripMenuItem.Text = "Экспорттаблиц в Excel";
            this.импортВExcelToolStripMenuItem.Click += new System.EventHandler(this.импортВExcelToolStripMenuItem_Click);
            // 
            // импортВWordToolStripMenuItem
            // 
            this.импортВWordToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.таблицыToolStripMenuItem1,
            this.ткущиеЗаписиToolStripMenuItem,
            this.записиToolStripMenuItem,
            this.записиПоСтандартуГОСТToolStripMenuItem});
            this.импортВWordToolStripMenuItem.Enabled = false;
            this.импортВWordToolStripMenuItem.Name = "импортВWordToolStripMenuItem";
            this.импортВWordToolStripMenuItem.Size = new System.Drawing.Size(247, 26);
            this.импортВWordToolStripMenuItem.Text = "Экспорт в Word";
            // 
            // таблицыToolStripMenuItem1
            // 
            this.таблицыToolStripMenuItem1.Name = "таблицыToolStripMenuItem1";
            this.таблицыToolStripMenuItem1.Size = new System.Drawing.Size(275, 26);
            this.таблицыToolStripMenuItem1.Text = "Таблицы";
            this.таблицыToolStripMenuItem1.Click += new System.EventHandler(this.таблицыToolStripMenuItem1_Click);
            // 
            // ткущиеЗаписиToolStripMenuItem
            // 
            this.ткущиеЗаписиToolStripMenuItem.Name = "ткущиеЗаписиToolStripMenuItem";
            this.ткущиеЗаписиToolStripMenuItem.Size = new System.Drawing.Size(275, 26);
            this.ткущиеЗаписиToolStripMenuItem.Text = "Диаграммы";
            this.ткущиеЗаписиToolStripMenuItem.Click += new System.EventHandler(this.диаграммыToolStripMenuItem_Click);
            // 
            // записиToolStripMenuItem
            // 
            this.записиToolStripMenuItem.Name = "записиToolStripMenuItem";
            this.записиToolStripMenuItem.Size = new System.Drawing.Size(275, 26);
            this.записиToolStripMenuItem.Text = "Записи по стандарту IEEE";
            this.записиToolStripMenuItem.Click += new System.EventHandler(this.записи_IEEE_ToolStripMenuItem_Click);
            // 
            // записиПоСтандартуГОСТToolStripMenuItem
            // 
            this.записиПоСтандартуГОСТToolStripMenuItem.Name = "записиПоСтандартуГОСТToolStripMenuItem";
            this.записиПоСтандартуГОСТToolStripMenuItem.Size = new System.Drawing.Size(275, 26);
            this.записиПоСтандартуГОСТToolStripMenuItem.Text = "Записи по стандарту ГОСТ";
            this.записиПоСтандартуГОСТToolStripMenuItem.Click += new System.EventHandler(this.записиПоСтандартуГОСТToolStripMenuItem_Click);
            // 
            // dsjlToolStripMenuItem
            // 
            this.dsjlToolStripMenuItem.Name = "dsjlToolStripMenuItem";
            this.dsjlToolStripMenuItem.Size = new System.Drawing.Size(250, 26);
            this.dsjlToolStripMenuItem.Text = "Выход";
            this.dsjlToolStripMenuItem.Click += new System.EventHandler(this.dsjlToolStripMenuItem_Click);
            // 
            // расширенныйПоискToolStripMenuItem
            // 
            this.расширенныйПоискToolStripMenuItem.Name = "расширенныйПоискToolStripMenuItem";
            this.расширенныйПоискToolStripMenuItem.Size = new System.Drawing.Size(108, 24);
            this.расширенныйПоискToolStripMenuItem.Text = "Фильтрация";
            this.расширенныйПоискToolStripMenuItem.Visible = false;
            this.расширенныйПоискToolStripMenuItem.Click += new System.EventHandler(this.расширенныйПоискToolStripMenuItem_Click);
            // 
            // сброситьПоискToolStripMenuItem1
            // 
            this.сброситьПоискToolStripMenuItem1.Name = "сброситьПоискToolStripMenuItem1";
            this.сброситьПоискToolStripMenuItem1.Size = new System.Drawing.Size(143, 24);
            this.сброситьПоискToolStripMenuItem1.Text = "Сбросить фильтр";
            this.сброситьПоискToolStripMenuItem1.Visible = false;
            this.сброситьПоискToolStripMenuItem1.Click += new System.EventHandler(this.сброситьПоискToolStripMenuItem1_Click);
            // 
            // диаграммаToolStripMenuItem
            // 
            this.диаграммаToolStripMenuItem.Name = "диаграммаToolStripMenuItem";
            this.диаграммаToolStripMenuItem.Size = new System.Drawing.Size(106, 24);
            this.диаграммаToolStripMenuItem.Text = "Диаграммы";
            this.диаграммаToolStripMenuItem.Visible = false;
            this.диаграммаToolStripMenuItem.Click += new System.EventHandler(this.диаграммаToolStripMenuItem_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.расширенныйПоискToolStripMenuItem,
            this.сброситьПоискToolStripMenuItem1,
            this.диаграммаToolStripMenuItem,
            this.таблицыToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(964, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // таблицыToolStripMenuItem
            // 
            this.таблицыToolStripMenuItem.Name = "таблицыToolStripMenuItem";
            this.таблицыToolStripMenuItem.Size = new System.Drawing.Size(85, 24);
            this.таблицыToolStripMenuItem.Text = "Таблицы";
            this.таблицыToolStripMenuItem.Visible = false;
            this.таблицыToolStripMenuItem.Click += new System.EventHandler(this.таблицыToolStripMenuItem_Click);
            // 
            // fakePanel
            // 
            this.fakePanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fakePanel.BackColor = System.Drawing.SystemColors.Info;
            this.fakePanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fakePanel.Controls.Add(this.label1);
            this.fakePanel.Location = new System.Drawing.Point(1, 1);
            this.fakePanel.Name = "fakePanel";
            this.fakePanel.Size = new System.Drawing.Size(937, 27);
            this.fakePanel.TabIndex = 4;
            this.fakePanel.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(4, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(204, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "Название публикации";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.fakePanel);
            this.panel1.Controls.Add(this.mainDGV);
            this.panel1.Location = new System.Drawing.Point(12, 31);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(940, 497);
            this.panel1.TabIndex = 2;
            // 
            // mainDGV
            // 
            this.mainDGV.AllowUserToAddRows = false;
            this.mainDGV.AllowUserToDeleteRows = false;
            this.mainDGV.AllowUserToResizeColumns = false;
            this.mainDGV.AllowUserToResizeRows = false;
            this.mainDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.mainDGV.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.mainDGV.BackgroundColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.mainDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.mainDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Azure;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.mainDGV.DefaultCellStyle = dataGridViewCellStyle5;
            this.mainDGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainDGV.EnableHeadersVisualStyles = false;
            this.mainDGV.Location = new System.Drawing.Point(0, 0);
            this.mainDGV.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.mainDGV.Name = "mainDGV";
            this.mainDGV.ReadOnly = true;
            this.mainDGV.RowHeadersVisible = false;
            this.mainDGV.RowHeadersWidth = 51;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.mainDGV.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.mainDGV.RowTemplate.Height = 24;
            this.mainDGV.Size = new System.Drawing.Size(940, 497);
            this.mainDGV.TabIndex = 1;
            this.mainDGV.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.mainDGV_CellMouseDoubleClick);
            this.mainDGV.Scroll += new System.Windows.Forms.ScrollEventHandler(this.mainDGV_Scroll);
            // 
            // labelRowsCount
            // 
            this.labelRowsCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelRowsCount.AutoSize = true;
            this.labelRowsCount.Location = new System.Drawing.Point(903, 9);
            this.labelRowsCount.Name = "labelRowsCount";
            this.labelRowsCount.Size = new System.Drawing.Size(16, 17);
            this.labelRowsCount.TabIndex = 3;
            this.labelRowsCount.Text = "0";
            this.labelRowsCount.Visible = false;
            // 
            // labelRowsInfo
            // 
            this.labelRowsInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelRowsInfo.AutoSize = true;
            this.labelRowsInfo.Location = new System.Drawing.Point(765, 9);
            this.labelRowsInfo.Name = "labelRowsInfo";
            this.labelRowsInfo.Size = new System.Drawing.Size(132, 17);
            this.labelRowsInfo.TabIndex = 4;
            this.labelRowsInfo.Text = "Всего побликаций:";
            this.labelRowsInfo.Visible = false;
            // 
            // formMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(964, 540);
            this.Controls.Add(this.labelRowsInfo);
            this.Controls.Add(this.labelRowsCount);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MinimumSize = new System.Drawing.Size(910, 587);
            this.Name = "formMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IEEE Systematizer";
            this.ResizeBegin += new System.EventHandler(this.MainForm_ResizeBegin);
            this.ResizeEnd += new System.EventHandler(this.MainForm_ResizeEnd);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.fakePanel.ResumeLayout(false);
            this.fakePanel.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainDGV)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem открытьНовыйToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem импортВExcelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem импортВWordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dsjlToolStripMenuItem;
        protected internal System.Windows.Forms.ToolStripMenuItem расширенныйПоискToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сброситьПоискToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem диаграммаToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Panel fakePanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView mainDGV;
        private System.Windows.Forms.Label labelRowsCount;
        private System.Windows.Forms.Label labelRowsInfo;
        private System.Windows.Forms.ToolStripMenuItem таблицыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem таблицыToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem записиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ткущиеЗаписиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem записиПоСтандартуГОСТToolStripMenuItem;
    }
}

