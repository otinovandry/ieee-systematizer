﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibStruct
{
    public partial class formProgress : Form
    {
        int steps; // кол-во шагов всего
        int currentStep = 0;
        public formProgress(int stepsCount)
        {
            InitializeComponent();
            RefreshText();
            steps = stepsCount > 1 ? stepsCount : 1;
            progressBar.Maximum = 100;
            progressBar.Step = 100 / steps;
            progressBar.Value = 0;   
        }

        public void RefreshText()
        {
            progressBar.CreateGraphics().DrawString("Пожалуйста подождите, идёт загрузка документа...",
                new Font("Microsoft Sans Serif", 11, FontStyle.Regular),
                Brushes.Black, new PointF(10, progressBar.Height / 2 - 7));
        }
        public void nextStep()
        {
            if (currentStep < steps)
            {
                progressBar.PerformStep();
                RefreshText();
            }
                
        }
    }
}
